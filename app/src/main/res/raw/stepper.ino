
#include <Arduino.h>
#include "BasicStepperDriver.h"
#include "MultiDriver.h"
 
#define EN        8  
#define Y_DIR     7
#define Y_STP     4 
#define X_DIR     6
#define X_STP     3 
#define MOTOR_STEPS 200
#define RPM 1
#define MICROSTEPSX 1
#define MICROSTEPSZ 1
#define STOPPER_TOP 9
#define STOPPER_BOTTOM 10

 BasicStepperDriver stepperY(MOTOR_STEPS, Y_DIR, Y_STP, EN);
 BasicStepperDriver stepperX(MOTOR_STEPS, X_DIR, X_STP, EN);
 MultiDriver controller(stepperX, stepperY);
String inString = "";    // string to hold input
long stpsX;
long stpsY;
  
  void move(){
    //allow small movement during tracking
    if(stpsX<50 and stpsX>-50 and stpsY<50 and stpsY>-50 and digitalRead(STOPPER_TOP) == HIGH and digitalRead(STOPPER_BOTTOM) == HIGH){
         stepperY.begin(RPM, MICROSTEPSZ);
         stepperX.begin(RPM, MICROSTEPSX);
        controller.move(stpsX,stpsY);
        stpsY=0;
        stpsX=0;
    }else{  
      long rpm=50;    
       while(stpsX!=0){ 
        if(stpsX>0){
          if(rpm<stpsX and rpm<300){
            //increasing speed
            rpm+=50;
            stepperX.begin(rpm, MICROSTEPSX);
            controller.move(50,0);
            stpsX-=50;
          }else if(rpm<stpsX){
            //stable max speed
            controller.move(100,0);
            stpsX-=100;
          }else if(stpsX>100){
            //slow down
            rpm-=50;
            stepperX.begin(rpm, MICROSTEPSX);
            controller.move(50,0);
            stpsX-=50;
            }else{
              //stop
              stepperX.begin(50, MICROSTEPSX);
              controller.move(stpsX,0);
              stpsX=0;
            }
        }else{
          if(rpm<-stpsX and rpm<300){
            //increasing speed
            rpm+=50;
            stepperX.begin(rpm, MICROSTEPSX);
            controller.move(-50,0);
            stpsX+=50;
          }else if(rpm<-stpsX){
            //stable max speed
            controller.move(-100,0);
             stpsX+=100;
          }else if(stpsX<-100){
            //slow down
            rpm-=50;
            stepperX.begin(rpm, MICROSTEPSX);
            controller.move(-50,0);
            stpsX+=50;
            }else{
              //stop
              stepperX.begin(50, MICROSTEPSX);
              controller.move(stpsX,0);
              stpsX=0;
            }
          }
       }       
       rpm=20;  
      while(stpsY!=0){ 
        if (digitalRead(STOPPER_TOP) == LOW and stpsY>0){
          Serial.print("limit up");
          stpsY=0;
        }
        if (digitalRead(STOPPER_BOTTOM) == LOW and stpsY<0){
          Serial.print("limit down");
          stpsY=0;
        }
    
        if(stpsY>0){
          if(rpm<stpsY and rpm<200){
            //increasing speed
            rpm+=10;
            stepperY.begin(rpm, MICROSTEPSZ);
            controller.move(0,10);
            stpsY-=10;
          }else if(rpm<stpsY){
            //stable max speed
            controller.move(0,100);
            stpsY-=100;
          }else if(stpsY>30){
            //slow down
            rpm-=10;
            stepperY.begin(rpm, MICROSTEPSZ);
            controller.move(0,10);
            stpsY-=10;
            }else{
              //stop
              stepperY.begin(20, MICROSTEPSZ);
              controller.move(0,stpsY);
              stpsY=0;
            }
        }else{
          if(rpm<-stpsY and rpm<200){
            //increasing speed
            rpm+=10;
            stepperY.begin(rpm, MICROSTEPSZ);
            controller.move(0,-10);
            stpsY+=10;
          }else if(rpm<-stpsY){
            //stable max speed
            controller.move(0,-100);
             stpsY+=100;
          }else if(stpsY<-30){
            //slow down
            rpm-=10;
            stepperY.begin(rpm, MICROSTEPSZ);
            controller.move(0,-10);
            stpsY+=10;
            }else{
              //stop
              stepperY.begin(20, MICROSTEPSZ);
              controller.move(0,stpsY);
              stpsY=0;
            }
          }
       }
    }
   stepperX.disable();
   stepperY.disable();
  }
void setup(){  
  Serial.begin(9600);  
  // Configure stopper pin to read HIGH unless grounded
    pinMode(STOPPER_TOP, INPUT_PULLUP); 
    pinMode(STOPPER_BOTTOM, INPUT_PULLUP);  
     stpsX=0;
     stpsY=0; 
}

void loop(){
//       if (digitalRead(STOPPER_TOP) == LOW){
//           moveUp=false;
//            Serial.print("limit up");
//       } else
//          moveUp=true;
//          
//        if (digitalRead(STOPPER_BOTTOM) == LOW){
//          moveDown=false;
//           Serial.print("limit down");
//        } else
//          moveDown=true;
        
    if (Serial.available() > 0) {
    int inChar = Serial.read();
    inString += (char)inChar;     
    if (inChar == '\n') {
      long steps=inString.substring(1).toInt(); 
      if(inString.startsWith("u")){
        Serial.print("Move up ");
        Serial.println(steps);
       stpsY=steps;
      }else if(inString.startsWith("d")){
        Serial.print("Move down ");
         Serial.println(steps);
        stpsY=-steps;
      }else if(inString.startsWith("l")){
        Serial.print("Move left ");
         Serial.println(steps);
         stpsX=steps;
      }else if(inString.startsWith("r")){
        Serial.print("Move right ");
         Serial.println(steps);
         stpsX=-steps;
      }else if(inString.startsWith("x")){
        Serial.println("Disable");
        stepperX.disable();
        stepperY.disable();
      }else if(inString.startsWith("e")){
        Serial.println("Execute");
        move(); 
      }
      inString = "";
    }
  }
}
