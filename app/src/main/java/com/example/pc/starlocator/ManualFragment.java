package com.example.pc.starlocator;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class ManualFragment extends Fragment {


    public ManualFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(int page) {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manual, container, false);

        final Spinner spinner = (Spinner) view.findViewById(R.id.spinnerMin);
        addItemsOnSpinner(spinner);

        final Button button_up = view.findViewById(R.id.button_up);
        button_up.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees=minutes/60;
                double min=(float)(minutes%60)/60;
                degrees += min;
                MainActivity.targetAltAz[1] += degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button button_left = view.findViewById(R.id.button_left);
        button_left.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees=minutes/60;
                double min=(float)(minutes%60)/60;
                degrees += min;
                MainActivity.targetAltAz[0] -= degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button button_right = view.findViewById(R.id.button_right);
        button_right.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees=minutes/60;
                double min=(float)(minutes%60)/60;
                degrees += min;
                MainActivity.targetAltAz[0] += degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button button_down = view.findViewById(R.id.button_down);
        button_down.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees=minutes/60;
                double min=(float)(minutes%60)/60;
                degrees += min;
                MainActivity.targetAltAz[1] -= degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button buttonReset = view.findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = 90;
                MainActivity.track = false;
                MainActivity.track();
            }
        });
        return view;
    }

    public void addItemsOnSpinner(Spinner spinner) {

// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.manual_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

}
