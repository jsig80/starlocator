package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Saturn extends Body {

	public Saturn(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}


	public Saturn(){
		super(
		        "Saturn", 
		        113.6634, 2.3898e-5, 2.4886, -1.081e-7, 339.3939, 2.97661e-5, 9.55475, 0.0, 0.055546, -9.499e-9, 316.9670, 0.0334442282,    // orbital elements
		        -9.0, 0.044, 0, 0     // visual magnitude elements
		    );   
	}
	
	@Override
	public void calcPosition(GeograficCoordinates location,Earth earth) {
		double day = AstrUtil.DayValue();
		eclipticCartesianCoordinates=calcEclipticCartesianCoordinates(day);
		eclipticCartesianCoordinates= perturb (eclipticCartesianCoordinates.x,
				eclipticCartesianCoordinates.y, eclipticCartesianCoordinates.z, day);
		eclipticAngularCoordinates=calcEclipticAngularCoordinates(day, eclipticCartesianCoordinates);
		geocentricCoordinates=calcGeocentricCoordinates(day, eclipticCartesianCoordinates,earth);
		eqCoords=calcEqCoords(day, location);
		horCoords=calcHorizontalCoordinates(day, location);
		this.setMagnitude(VisualMagnitude(AstrUtil.DayValue()));
	}
	
	@Override
	public CartesianCoordinates perturb(double xh, double yh, double zh, double d) {
		SphericalCoordinates ecliptic = AstrUtil.EclipticLatLon(xh, yh, zh);
		double lonecl = ecliptic.longitude;
		double latecl = ecliptic.latitude;

		double r = Math.sqrt(xh * xh + yh * yh + zh * zh);
		double Mj = AstrUtil.jupiterMeanAnomaly(d);
		double Ms = AstrUtil.saturnMeanAnomaly(d);

		lonecl += 0.812 * AstrUtil.SinDeg(2 * Mj - 5 * Ms - 67.6) - 0.229 * AstrUtil.CosDeg(2 * Mj - 4 * Ms - 2.0)
				+ 0.119 * AstrUtil.SinDeg(Mj - 2 * Ms - 3.0) + 0.046 * AstrUtil.SinDeg(2 * Mj - 6 * Ms - 69.0)
				+ 0.014 * AstrUtil.SinDeg(Mj - 3 * Ms + 32.0);
		latecl += -0.020 * AstrUtil.CosDeg(2 * Mj - 4 * Ms - 2) + 0.018 * AstrUtil.SinDeg(2 * Mj - 6 * Ms - 49);

		double coslon = AstrUtil.CosDeg(lonecl);
		double sinlon = AstrUtil.SinDeg(lonecl);
		double coslat = AstrUtil.CosDeg(latecl);
		double sinlat = AstrUtil.SinDeg(latecl);

		double xp = r * coslon * coslat;
		double yp = r * sinlon * coslat;
		double zp = r * sinlat;

		return new CartesianCoordinates(xp, yp, zp);
	}

	@Override
	public double VisualMagnitude(double day) {
		double planetMagnitude = super.VisualMagnitude(day); // get magnitude of
															// the planet body
															// itself.

		double Ir = 28.06; // inclination of Saturn's rings to ecliptic, in
							// degrees
		double cosIr = AstrUtil.CosDeg(Ir);
		double sinIr = AstrUtil.SinDeg(Ir);

		CartesianCoordinates gc = geocentricCoordinates;
		double Los = AstrUtil.FixDegrees(AstrUtil.AtanDeg2(gc.y, gc.x));
		double Las = AstrUtil.FixDegrees(AstrUtil.AtanDeg2(gc.z, Math.sqrt(gc.x * gc.x + gc.y * gc.y)));
		double sinLas = AstrUtil.SinDeg(Las);
		double cosLas = AstrUtil.CosDeg(Las);
		double Nr = 169.51 + (3.82e-5 * day); // ascending node of the plane of
												// Saturn's rings
		double sinLosMinusNr = AstrUtil.SinDeg(Los - Nr);

		double B = Math.asin(sinLas * cosIr - cosLas * sinIr * sinLosMinusNr);
		double sinB = Math.abs(Math.sin(B)); // ??? can we get rid of doing both
												// Asin and Sin?

		double ringMagnitude = (-2.6 * Math.abs(sinB)) + (1.2 * sinB * sinB);
		return planetMagnitude + ringMagnitude;
	}

}
