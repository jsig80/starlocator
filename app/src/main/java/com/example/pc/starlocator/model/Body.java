package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Body{

	protected String commonName;
	private String catalogId;
	private String constellation;
	private String type;
	private String secType;
	private double magnitude;
	private int epoch;
//	private double[] rAHours;
//	private double[] decl;
	private double raDegreesDecimal;
	//private double raHoursDecimal;
	private double declDecimal;
	private double size;

	private double N0;
	private double Nc;
	private double i0;
	private double ic;
	private double w0;
	private double wc;
	private double magBase;
	private double magPhaseFactor;
	private double magNonlinearFactor;
	private double magNonlinearExponent;
	private double Mc;
	private double M0;
	private double ec;
	private double e0;
	private double ac;
	private double a0;
	

	protected CartesianCoordinates eclipticCartesianCoordinates;//x,y,z
	protected SphericalCoordinates eclipticAngularCoordinates;//long,lat
	protected CartesianCoordinates geocentricCoordinates;//swift x,y,z to geocentric
	protected SphericalCoordinates eqCoords;//ra,dec
	protected HorizontalCoordinates horCoords;//alt az

	public Body() {
	}
	
	public String toString(){
		if(catalogId!=null && commonName!=null)
			return catalogId +" "+commonName;
		else if(catalogId!=null)
			return catalogId;
		else
			return commonName;
	}
	
	public void setBodyData(       //  Designed for use with JPL orbital data
		    String name,       // name of the object, e.g. "Mars"
		    double ja0, double jac,   // semi-major axis (AU), rate (AU/century)
		    double je0,double jec,   // eccentricity (1), (1/century)
		    double jI0,double jIc,   // inclination (deg), (deg/century)
		    double jL0,double  jLc,   // mean longitude (deg), (deg/century)
		    double ju0, double juc,   // longitude of perihelion (deg), (deg/century)
		    double  jQ0, double jQc,   // longitude of ascending node (deg), (deg/century)
		    double magBase,
		    double magPhaseFactor,
		    double magNonlinearFactor,
		    double magNonlinearExponent )
		{
		    // Convert units and epoch from JPL format to PlanetPS format.
		    // http://ssd.jpl.nasa.gov/txt/aprx_pos_planets.pdf
		    // http://ssd.jpl.nasa.gov/txt/p_elem_t1.txt
		    double dpc = 36525.0;      // days per century
		    double cofs = 1.5/dpc;     // centuries that JPL epoch (J2000 = 1/1/2000 12UT) is ahead of PS (12/31/1999 0UT)
		    
		    double N0 = jQ0 - cofs*jQc;
		    double Nc = jQc/dpc;
		    
		    double i0 = jI0 - cofs*jIc;
		    double ic = jIc/dpc;
		    
		    // w = ju - jQ
		    double w0 = AstrUtil.FixDegrees((ju0 - cofs*juc) - (jQ0 - cofs*jQc));
		    double wc = (juc - jQc)/dpc;
		    
		    double a0 = ja0 - cofs*jac;
		    double ac = jac/dpc;
		    
		    double e0 = je0 - cofs*jec;
		    double ec = jec/dpc;
		    
		    // M = jL - ju
		    double M0 = AstrUtil.FixDegrees((jL0 - cofs*jLc) - (ju0 - cofs*juc));
		    double Mc = (jLc - juc)/dpc;

		    this.commonName = name;
			this.type = "planet";
			this.N0 = N0;
			this.Nc = Nc;
			this.i0 = i0;
			this.ic = ic;
			this.w0 = w0;
			this.wc = wc;
			this.a0 = a0;
			this.ac = ac;
			this.e0 = e0;
			this.ec = ec;
			this.M0 = M0;
			this.Mc = Mc;
			this.magBase = magBase;
			this.magPhaseFactor = magPhaseFactor;
			this.magNonlinearFactor = magNonlinearFactor;
			this.magNonlinearExponent = magNonlinearExponent;
		}
	public void calcPosition(GeograficCoordinates location,Earth earth) {
		double day = AstrUtil.DayValue();
		eclipticCartesianCoordinates=calcEclipticCartesianCoordinates(day);
		eclipticCartesianCoordinates= perturb (eclipticCartesianCoordinates.x,
				eclipticCartesianCoordinates.y, eclipticCartesianCoordinates.z, day);
		eclipticAngularCoordinates=calcEclipticAngularCoordinates(day, eclipticCartesianCoordinates);
		geocentricCoordinates=calcGeocentricCoordinates(day, eclipticCartesianCoordinates,earth);
		eqCoords=calcEqCoords(day, location);
		horCoords=calcHorizontalCoordinates(day, location);
		this.setMagnitude(VisualMagnitude(AstrUtil.DayValue()));
	}
	
	public Body( // See http://www.stjarnhimlen.se/comp/ppcomp.html#4
			String name, // name of the object, e.g. "Mars"
			double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac, double e0,
			double ec, double M0, double Mc, double magBase, double magPhaseFactor, double magNonlinearFactor,
			double magNonlinearExponent) {
		this.commonName = name;
		this.type = "planet";
		this.N0 = N0;
		this.Nc = Nc;
		this.i0 = i0;
		this.ic = ic;
		this.w0 = w0;
		this.wc = wc;
		this.a0 = a0;
		this.ac = ac;
		this.e0 = e0;
		this.ec = ec;
		this.M0 = M0;
		this.Mc = Mc;
		this.magBase = magBase;
		this.magPhaseFactor = magPhaseFactor;
		this.magNonlinearFactor = magNonlinearFactor;
		this.magNonlinearExponent = magNonlinearExponent;
	}

	public double getN0() {
		return N0;
	}

	public void setN0(double n0) {
		N0 = n0;
	}

	public double getNc() {
		return Nc;
	}

	public void setNc(double nc) {
		Nc = nc;
	}

	public double getI0() {
		return i0;
	}

	public void setI0(double i0) {
		this.i0 = i0;
	}

	public double getIc() {
		return ic;
	}

	public void setIc(double ic) {
		this.ic = ic;
	}

	public double getW0() {
		return w0;
	}

	public void setW0(double w0) {
		this.w0 = w0;
	}

	public double getWc() {
		return wc;
	}

	public void setWc(double wc) {
		this.wc = wc;
	}

	public double getMagBase() {
		return magBase;
	}

	public void setMagBase(double magBase) {
		this.magBase = magBase;
	}

	public double getMagPhaseFactor() {
		return magPhaseFactor;
	}

	public void setMagPhaseFactor(double magPhaseFactor) {
		this.magPhaseFactor = magPhaseFactor;
	}

	public double getMagNonlinearFactor() {
		return magNonlinearFactor;
	}

	public void setMagNonlinearFactor(double magNonlinearFactor) {
		this.magNonlinearFactor = magNonlinearFactor;
	}

	public double getMagNonlinearExponent() {
		return magNonlinearExponent;
	}

	public void setMagNonlinearExponent(double magNonlinearExponent) {
		this.magNonlinearExponent = magNonlinearExponent;
	}

	public double getMc() {
		return Mc;
	}

	public void setMc(double mc) {
		Mc = mc;
	}

	public double getM0() {
		return M0;
	}

	public void setM0(double m0) {
		M0 = m0;
	}

	public double getEc() {
		return ec;
	}

	public void setEc(double ec) {
		this.ec = ec;
	}

	public double getE0() {
		return e0;
	}

	public void setE0(double e0) {
		this.e0 = e0;
	}

	public double getAc() {
		return ac;
	}

	public void setAc(double ac) {
		this.ac = ac;
	}

	public double getA0() {
		return a0;
	}

	public void setA0(double a0) {
		this.a0 = a0;
	}

	// day = number of days elapsed since December 31, 1999 0:00 UTC, i.e. JD
	// 2451543.5
	public double getMeanAnomaly(double day) {
		return this.M0 + (day * this.Mc);
	}

	public double getNodeLongitude(double day) {
		return this.N0 + (day * this.Nc);
	}

	public double getPerihelion(double day) {
		return this.w0 + (day * this.wc);
	}

	public CartesianCoordinates calcEclipticCartesianCoordinates(double day) {
		double a = this.a0 + (day * this.ac);
		double e = this.e0 + (day * this.ec);
		double M = this.M0 + (day * this.Mc);
		double N = this.N0 + (day * this.Nc);
		double w = this.w0 + (day * this.wc);
		double i = this.i0 + (day * this.ic);
		double E = AstrUtil.EccentricAnomaly(e, M);

		// Calculate the body's position in its own orbital plane, and its
		// distance from the thing it is orbiting.
		double xv = a * (AstrUtil.CosDeg(E) - e);
		double yv = a * (Math.sqrt(1.0 - e * e) * AstrUtil.SinDeg(E));

		double v = AstrUtil.AtanDeg2(yv, xv);
		// True anomaly in degrees: the angle from perihelion of the body as
		// seen by the Sun.
		double r = Math.sqrt(xv * xv + yv * yv);
		// Distance from the Sun to the planet in AU

		double cosN = AstrUtil.CosDeg(N);
		double sinN = AstrUtil.SinDeg(N);
		double cosi = AstrUtil.CosDeg(i);
		double sini = AstrUtil.SinDeg(i);
		double cosVW = AstrUtil.CosDeg(v + w);
		double sinVW = AstrUtil.SinDeg(v + w);

		// Now we are ready to calculate (unperturbed) ecliptic cartesian
		// heliocentric coordinates.
		double xh = r * (cosN * cosVW - sinN * sinVW * cosi);
		double yh = r * (sinN * cosVW + cosN * sinVW * cosi);
		double zh = r * sinVW * sini;
		
		return new CartesianCoordinates(xh, yh, zh);
	}

	public CartesianCoordinates calcGeocentricCoordinates(double day, CartesianCoordinates ccoords,Earth earth) 
	{
		CartesianCoordinates ec = earth.getEclipticCartesianCoordinates(); 
		return ccoords.Subtract(ec); // subtract vectors to get the vector from Earth
								// to this body
	}

	public SphericalCoordinates calcEclipticAngularCoordinates(double day,CartesianCoordinates ccoords) {
		SphericalCoordinates ac = AstrUtil.Polar(ccoords.x, ccoords.y, ccoords.z);
		return ac;
	}

	public HorizontalCoordinates calcHorizontalCoordinates(double day, GeograficCoordinates location) {
		SphericalCoordinates sky = eqCoords;
		return calcHorizontalCoordinates(sky, location, day, 0.0);
	}

	public SphericalCoordinates calcEqCoords(double day, GeograficCoordinates location) {
		CartesianCoordinates vectorFromEarth = geocentricCoordinates;
		double dx = vectorFromEarth.x;
		double dy = vectorFromEarth.y;
		double dz = vectorFromEarth.z;

		// Now (dx,dy,dz) comprise the vector from the Earth to the object in
		// cartesian ecliptic coordinates.
		// We convert here to equatorial coordinates, using formulas based on
		// the precession of the Earth's axis of rotation.
		double T = AstrUtil.CenturiesSinceJ2000(day);
		double K = 23.4392911 - ((46.8150 * T) - (0.00059 * T * T) + (0.001813 * T * T * T)) / 3600.0; // obliquity
																										// degrees.
		double cosK = AstrUtil.CosDeg(K);
		double sinK = AstrUtil.SinDeg(K);

		// Calculate equatorial cartesian coordinates using ecliptic cartesian
		// coordinates...
		double qx = dx;
		double qy = (dy * cosK) - (dz * sinK);
		double qz = (dy * sinK) + (dz * cosK);

		SphericalCoordinates eq = AstrUtil.Polar(qx, qy, qz);
		eq.longitude /= 15.0; // convert degrees to sidereal hours

//		double DEC = eq.latitude;
//		double RA = eq.longitude;
		double distanceInAU = eq.radius;

		// Determine whether a topocentric correction is warranted.
		// Imagine the angle between the the center of the Earth (geocentric),
		// the remote body,
		// and the observer on the surface of the Earth (topocentric).
		// If this angle is less than 1 arcminute, we won't bother with the
		// extra calculation time.
		// Use approximation to determine whether the parallax matters:
		// asin(RE/r) >= 1" where RE = radius of Earth, r = distance to
		// CelestialBody.
		// RE/r >= pi / (180 * 3600)
		double parallaxInRadians = 1.0 / (AstrUtil.EARTH_RADII_PER_ASTRONOMICAL_UNIT * distanceInAU);
		if (parallaxInRadians >= Math.PI / (180.0 * 3600.0)) {
			// We were using the approximation that parallax = asin(parallax).
			// Now that we know the parallax is significant, go ahead and
			// calculate the exact angle.
			parallaxInRadians = Math.asin(parallaxInRadians);

			// It is easier to calculate topocentric correction in horizontal
			// coordinates...
			//astronomy.HorizontalCoordinates hor = calcHorizontalCoordinates(eq, location, day, 0.0);

			double gclat;
			double rho;

				gclat = AstrUtil.OblateLatitudeCorrection(location.latitude);
				rho = AstrUtil.OblateRadiusCorrection(location.latitude);			

//			double altitude = hor.getAltitude()
//					- ((AstrUtil.DEG_FROM_RAD * parallaxInRadians) * AstrUtil.CosDeg(hor.getAltitude()));
			double Ls = AstrUtil.MeanLongitudeOfSun(day);
			double GMST0 = (Ls + 180.0) / 15.0;
			double utcHours = (day - Math.floor(day)) * 24.0;
			double LST = GMST0 + utcHours + (location.longitude / 15.0);
			double hourAngle = LST - eq.longitude;
			double g = AstrUtil.DEG_FROM_RAD * (Math.atan(AstrUtil.TanDeg(gclat) / AstrUtil.CosHour(hourAngle)));

			double topRA = eq.longitude - ((parallaxInRadians * AstrUtil.HOURS_FROM_RAD) * rho * AstrUtil.CosDeg(gclat)
					* AstrUtil.SinHour(hourAngle) / AstrUtil.CosDeg(eq.latitude));
			double topDEC;

			if (Math.abs(g) < 1.0e-6) {
				topDEC = eq.latitude - ((parallaxInRadians * AstrUtil.DEG_FROM_RAD) * rho
						* AstrUtil.SinDeg(-eq.latitude) * AstrUtil.CosHour(hourAngle));
			} else {
				topDEC = eq.latitude - (parallaxInRadians * AstrUtil.DEG_FROM_RAD) * rho * AstrUtil.SinDeg(gclat)
						* AstrUtil.SinDeg(g - eq.latitude) / AstrUtil.SinDeg(g);
			}

			eq = new SphericalCoordinates(topRA, topDEC, distanceInAU);
		}
		return eq;
	}

	public HorizontalCoordinates calcHorizontalCoordinates(SphericalCoordinates sky, GeograficCoordinates location,
			double day, double horizonCorrectionInArcMinutes) {
		// http://en.wikipedia.org/wiki/Horizontal_coordinate_system

		if (horizonCorrectionInArcMinutes == 0) {
			horizonCorrectionInArcMinutes = -34.0;
		}

		double GST = AstrUtil.GreenwichSiderealTimeInHours(day);
		double LST = GST + (location.longitude / 15.0);
		double hourAngle = AstrUtil.FixHours(LST - sky.longitude);

		double sinLat = AstrUtil.SinDeg(location.latitude);
		double cosLat = AstrUtil.CosDeg(location.latitude);

		double sinHourAngle = AstrUtil.SinHour(hourAngle);
		double cosHourAngle = AstrUtil.CosHour(hourAngle);

		double sinDec = AstrUtil.SinDeg(sky.latitude);
		double cosDec = AstrUtil.CosDeg(sky.latitude);
		double altitude = 0;
		double azimuth = 0;
		double altitudeRatio = (sinLat * sinDec) + (cosLat * cosDec * cosHourAngle);
		// Correct for values that are out of range for inverse sine function...
		double absAltitudeRatio = Math.abs(altitudeRatio);
		if (absAltitudeRatio > 1.0) {
			if (absAltitudeRatio > 1.000001) {
				// This is an excessive amount of error: something must be wrong
				// with the formula!
				// "Internal error: altitude would be a complex number!");
			} else {
				// Just correct for apparent roundoff without going bezerk.
				altitude = (altitudeRatio < 0) ? -90.0 : +90.0;
				azimuth = 180.0; // doesn't really matter what angle we assign:
									// use this value to assist culmination
									// algorithm.
			}
		} else {
			altitude = AstrUtil.DEG_FROM_RAD * Math.asin(altitudeRatio);
			double absAltitude = Math.abs(altitude);
			double ANGLE_CORRECTION_WINDOW = 6.0; // I chose 6 degrees as the
													// refraction cutoff,
													// because that is used for
													// Civil Twilight, which
													// should not be corrected.
			if ((absAltitude < ANGLE_CORRECTION_WINDOW) && (horizonCorrectionInArcMinutes != 0.0)) {
				// http://www.jgiesen.de/refract/index.html For more technical
				// details.
				// Correct for atmospheric lensing making the object appear
				// higher than it would if the Earth had no atmosphere.
				// We want the correction to maximize its effect at the horizon,
				// and vanish to zero as |altitude| approaches
				// ANGLE_CORRECTION_WINDOW.
				// For simplicity we apply a linear interpolation within this
				// range of altitudes.
				double linearFactor = (ANGLE_CORRECTION_WINDOW - absAltitude) / ANGLE_CORRECTION_WINDOW;
				altitude -= (horizonCorrectionInArcMinutes / 60.0) * linearFactor;
			}

			azimuth = AstrUtil.FixDegrees(
					AstrUtil.AtanDeg2(-cosDec * sinHourAngle, (cosLat * sinDec) - (sinLat * cosDec * cosHourAngle)));
		}
		HorizontalCoordinates coord = new HorizontalCoordinates();
		coord.setAltitude(altitude);
		coord.setAzimuth(azimuth);
		return coord;
	}

	public CartesianCoordinates perturb(double xh, double yh, double zh, double day) {
		// By default we apply no perturbations.
		// Some planets will override this method so that they can correct for
		// the gravitational influences of other planets.
		return new CartesianCoordinates(xh, yh, zh);
	}

	public double DistanceFromEarth() 
	{
		return geocentricCoordinates.Distance();
	}

	public double DistanceFromSun() {
		return eclipticCartesianCoordinates.Distance();
	}

	public double VisualMagnitude(double day) {
		double distEarth = this.DistanceFromEarth();
		double distSun = this.DistanceFromSun();
		double phase = AstrUtil.SunEarthPhaseAngle(this, day);
		double mag = this.magBase + (5 * Math.log10(distSun * distEarth)) + (this.magPhaseFactor * phase);
		if (this.magNonlinearExponent > 0) {
			mag += this.magNonlinearFactor * Math.pow(phase, this.magNonlinearExponent);
		}
		return mag;
	}

	public HorizontalCoordinates getHorCoords() {
		return horCoords;
	}

	public void setHorCoords(HorizontalCoordinates horCoords) {
		this.horCoords = horCoords;
	}

	public CartesianCoordinates getGeocentricCoordinates() {
		return geocentricCoordinates;
	}

	public void setGeocentricCoordinates(CartesianCoordinates geocentricCoordinates) {
		this.geocentricCoordinates = geocentricCoordinates;
	}

	public SphericalCoordinates getEclipticAngularCoordinates() {
		return eclipticAngularCoordinates;
	}

	public void setEclipticAngularCoordinates(SphericalCoordinates eclipticAngularCoordinates) {
		this.eclipticAngularCoordinates = eclipticAngularCoordinates;
	}

	public SphericalCoordinates getEqCoords() {
		return eqCoords;
	}

	public void setEqCoords(SphericalCoordinates eqCoords) {
		this.eqCoords = eqCoords;
	}

	public CartesianCoordinates getEclipticCartesianCoordinates() {
		return eclipticCartesianCoordinates;
	}

	public void setEclipticCartesianCoordinates(CartesianCoordinates eclipticCartesianCoordinates) {
		this.eclipticCartesianCoordinates = eclipticCartesianCoordinates;
	}
	public String getCommonName() {
		return commonName;
	}
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
	public String getConstellation() {
		return constellation;
	}
	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSecType() {
		return secType;
	}
	public void setSecType(String secType) {
		this.secType = secType;
	}
	public double getMagnitude() {
		return magnitude;
	}
	public void setMagnitude(double magnitude) {
		this.magnitude = magnitude;
	}
	public int getEpoch() {
		return epoch;
	}
	public void setEpoch(int epoch) {
		this.epoch = epoch;
	}
//	public double[] getrAHours() {
//		return rAHours;
//	}
//	public void setrAHours(double[] rAHours) {
//		this.rAHours = rAHours;
//	}
//	public double[] getDecl() {
//		return decl;
//	}
//	public void setDecl(double[] decl) {
//		this.decl = decl;
//	}
	public double getRaDegreesDecimal() {
		return raDegreesDecimal;
	}
	public void setRaDegreesDecimal(double raDegreesDecimal) {
		this.raDegreesDecimal = raDegreesDecimal;
	}
//	public double getRaHoursDecimal() {
//		return raHoursDecimal;
//	}
//	public void setRaHoursDecimal(double raHoursDecimal) {
//		this.raHoursDecimal = raHoursDecimal;
//	}
	public double getDeclDecimal() {
		return declDecimal;
	}
	public void setDeclDecimal(double declDecimal) {
		this.declDecimal = declDecimal;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
}
