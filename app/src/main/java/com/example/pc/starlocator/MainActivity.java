package com.example.pc.starlocator;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.example.pc.starlocator.Listeners.SimpleFragmentPagerAdapter;
import com.example.pc.starlocator.model.Body;
import com.example.pc.starlocator.model.Earth;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

public class MainActivity extends AppCompatActivity {

    static List<Body> namedStars;
    static List<Body> planets;

    static double[] obsLocation = new double[3];
    static double[] curAltAz = new double[2];
    static double[] targetAltAz = new double[2];
    static double[] targetRaDecl = new double[2];
    static String message;
    static boolean track = false;
    //public static boolean calculate = false;
    static double altStepsDegree = 129.44;
    static double azStepsDegree = 592.77;
    static int pressure = 1000;
    static int temperature = 20;
    static Earth earth;
    //static double[][] tMatrix = new double[3][3];
    //static double t0;//time of first allignment star in radians
    static SQLiteDatabase mydatabase;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    AlertDialog alert;
    static ViewPager2 viewPager;
    Thread listsThread;
    volatile boolean stopWorker = false;
    byte[] readBuffer;
    int readBufferPosition;
    static boolean changedTheme;
    static List<Double[]> allignStars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (changedTheme)
            setTheme(R.style.darkTheme);
        else
            setTheme(R.style.lightTheme);

        setContentView(R.layout.activity_main);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        allignStars = new ArrayList<>();
        obsLocation[0] = 23.843;
        obsLocation[1] = 38.014;
        obsLocation[2] = 240;

        curAltAz[0] = 0;
        curAltAz[1] = 90;
        earth = new Earth();
        mydatabase = openOrCreateDatabase("starDb", MODE_PRIVATE, null);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS StarTbl(catalogueId INTEGER PRIMARY KEY,starName VARCHAR,magnitude REAL,ra REAL,decl REAL);");

        //check for list and fill db first time
        Cursor resultSet = mydatabase.rawQuery("Select * from StarTbl ", null);
        int count = resultSet.getCount();
        if (count < 117000) {
            alert = new AlertDialog.Builder(this).create();
            alert.setTitle("Time for coffee, Hipparchus data loading");
            alert.show();
            loadLsts();
        }

        //db is full, fill list
        namedStars = new ArrayList<>();
        resultSet = mydatabase.rawQuery("Select * from StarTbl where starName is not null order by starName COLLATE UNICODE", null);
        int iRow = resultSet.getColumnIndex("catalogueId");
        int iName = resultSet.getColumnIndex("starName");
        int iMagn = resultSet.getColumnIndex("magnitude");
        int iDecl = resultSet.getColumnIndex("decl");
        int iRa = resultSet.getColumnIndex("ra");

        for (resultSet.moveToFirst(); !resultSet.isAfterLast(); resultSet.moveToNext()) {
            Body star = new Body();
            star.setCatalogId(resultSet.getString(iRow));
            star.setCommonName(resultSet.getString(iName));
            star.setMagnitude(resultSet.getDouble(iMagn));
            star.setRaDegreesDecimal(resultSet.getDouble(iRa));
            star.setDeclDecimal(resultSet.getDouble(iDecl));
            namedStars.add(star);
        }

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new SimpleFragmentPagerAdapter(this));

        String tabTitles[] = new String[]{"Settings", "Tracking"};

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> tab.setText(tabTitles[position])).attach();

//        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
//            @Override
//            public void onPageSelected(int position) {
//                super.onPageSelected(position);
//                if (position == 1)
//                    calculate = true;
//                else
//                    calculate = false;
//            }
//        });
    }

    public void restart() {
        closeBT();
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public static List<Body> queryList(String text) {
        List<Body> hipStars = new ArrayList<>();
        String[] array = new String[1];
        if (text.contains(" "))
            array[0] = text.substring(0, text.indexOf(" "));
        else
            array[0] = text;

        Cursor resultSet = mydatabase.rawQuery("Select * from StarTbl where catalogueId = :query LIMIT 10 COLLATE UNICODE", array);
        int iRow = resultSet.getColumnIndex("catalogueId");
        int iName = resultSet.getColumnIndex("starName");
        int iMagn = resultSet.getColumnIndex("magnitude");
        int iDecl = resultSet.getColumnIndex("decl");
        int iRa = resultSet.getColumnIndex("ra");

        for (resultSet.moveToFirst(); !resultSet.isAfterLast(); resultSet.moveToNext()) {
            Body star = new Body();
            star.setCatalogId(resultSet.getString(iRow));
            star.setCommonName(resultSet.getString(iName));
            star.setMagnitude(resultSet.getDouble(iMagn));
            star.setRaDegreesDecimal(resultSet.getDouble(iRa));
            star.setDeclDecimal(resultSet.getDouble(iDecl));
            hipStars.add(star);
        }
        return hipStars;
    }

    public String connectBlt() throws IOException {

        boolean bluetoothConnect = false;

        if (mmSocket != null && mmSocket.isConnected())
            bluetoothConnect = true;
        else if (findBT()) {
            openBT();
            bluetoothConnect = true;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (bluetoothConnect) {
            beginSendData();
            return "Bluetooth connected";
        } else {
            return "Bluetooth is not connected";
        }
    }

    public static void track() {
        try {
            double moveAz = targetAltAz[0] - curAltAz[0];
            double moveAlt = targetAltAz[1] - curAltAz[1];

            if (moveAz > 180)
                moveAz = moveAz - 360;
            if (moveAz < -180)
                moveAz = 360 + moveAz;

            long stepsAz = Math.round(Math.abs(moveAz) * azStepsDegree);
            long stepsAlt = Math.round(Math.abs(moveAlt) * altStepsDegree);

            if (stepsAlt > 2) {
                if (moveAlt > 0)
                    message = "u" + stepsAlt + "\n";
                else
                    message = "d" + stepsAlt + "\n";
                curAltAz[1] = targetAltAz[1];
                // wait for message to send
                Thread.sleep(200);
            }
            if (stepsAz > 10) {
                if (moveAz > 0)
                    message = "r" + stepsAz + "\n";
                else
                    message = "l" + stepsAz + "\n";

                curAltAz[0] = targetAltAz[0];
                // wait for message to send
                Thread.sleep(200);
            }
            if(stepsAz>10 ||stepsAlt>2) {
                message = "e\n";
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean findBT() {
        boolean result = false;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("HC-05")) {
                    mmDevice = device;
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public void openBT() throws IOException {
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            stopWorker = false;
    }

    public void beginSendData() {
        workerThread = new Thread(new Runnable() {
            public void run() {
                int tries = 0;
                while (!Thread.currentThread().isInterrupted() && !stopWorker && tries < 10) {
                    try {
                        if (message != null) {
                            mmOutputStream.write(message.getBytes());
                            //System.out.print("Sent a signal. ");
                            System.out.println(message);
                            message = null;
                        }
                        Thread.sleep(100);
                    } catch (IOException | InterruptedException e) {
                        tries++;
                        try {
                            openBT();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        e.printStackTrace();
                    }
                }
            }
        });

        workerThread.start();
    }

    public void loadLsts() {
        listsThread = new Thread(new Runnable() {
            public void run() {
                mydatabase.delete("StarTbl", "", null);
                List<Body> hipStars = Parser.doParseHip(getApplicationContext());
                for (Body star : hipStars) {
                    double ra = star.getRaDegreesDecimal();
                    double decl = star.getDeclDecimal();
                    String name = star.getCommonName();
                    String id = star.getCatalogId();
                    double magn = star.getMagnitude();
                    // Create a new map of values, where column names are the keys
                    ContentValues values = new ContentValues();
                    values.put("starName", name);
                    values.put("catalogueId", id);
                    values.put("magnitude", magn);
                    values.put("decl", decl);
                    values.put("ra", ra);
                    long newRowId = mydatabase.insert("StarTbl", null, values);
                }
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert.hide();
                    }
                });
            }
        });

        listsThread.start();
    }
//    public String sendData(String msg) throws IOException
//    {
//        msg += "\n";
//        mmOutputStream.write(msg.getBytes());
//        return "Data Sent";
//    }

    @Override
    protected void onStop() {
        // call the superclass method first
        super.onStop();
        closeBT();
    }

    public String closeBT() {
        if (mmSocket != null && mmSocket.isConnected()) {
            try {
                stopWorker = true;
                mmOutputStream.close();
                mmInputStream.close();
                mmSocket.close();
            } catch (IOException ex) {
            }
        }
        return "Bluetooth Closed";
    }
}