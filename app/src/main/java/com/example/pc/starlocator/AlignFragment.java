package com.example.pc.starlocator;


import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.pc.starlocator.model.Body;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class AlignFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    TextView lbl1,lbl5,lbl6;
    Button buttonAlign,buttonGoTo,buttonClear;
    Spinner spinnerStar;
 //   SwitchCompat simpleSwitch;
//    double[] star1 = new double[5];
//    double[] star2 = new double[5];
//    double[] star3 = new double[5];
    Body allignStar;
    int step = 0;
  //  boolean threeStar=false;

    public AlignFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(int page) {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_align, container, false);

        final Spinner spinner = (Spinner) view.findViewById(R.id.spinnerMin);
        addItemsOnSpinner(spinner);

        spinnerStar = (Spinner) view.findViewById(R.id.spinner1);
        addStarOnSpinner(spinnerStar);
        spinnerStar.setOnItemSelectedListener(this);
        spinnerStar.setEnabled(false);
        lbl1 = view.findViewById(R.id.lbl1);
        lbl1.setText("Select First Star");
        lbl5 = view.findViewById(R.id.lbl5);
        lbl6 = view.findViewById(R.id.lbl6);

        final Button button_up = view.findViewById(R.id.button_up);
        button_up.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[1] += degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button button_left = view.findViewById(R.id.button_left);
        button_left.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[0] -= degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button button_right = view.findViewById(R.id.button_right);
        button_right.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[0] += degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        final Button button_down = view.findViewById(R.id.button_down);
        button_down.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[1] -= degrees;
                MainActivity.track = false;
                MainActivity.track();
            }
        });

        buttonGoTo = view.findViewById(R.id.button_go);
        buttonGoTo.setEnabled(false);
        buttonGoTo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.track();
                buttonGoTo.setEnabled(false);
            }
        });

        buttonClear = view.findViewById(R.id.button_clear);
        buttonClear.setEnabled(false);
        buttonClear.setText("Clear Alignment");
        buttonClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.allignStars=new ArrayList<>();
                buttonClear.setEnabled(false);
                buttonAlign.setText("Begin Alignment");
                step = 0;
            }
        });

        buttonAlign = view.findViewById(R.id.button_align);
        buttonAlign.setText("Begin Alignment");
        buttonAlign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Date now = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                float ut = cal.get(Calendar.HOUR_OF_DAY);
                float min = cal.get(Calendar.MINUTE);
                float sec = cal.get(Calendar.SECOND);
                float dec = ((min * 60) + sec) / 3600;
                ut += dec;
                double tRad = CoordConvert.toRadians((ut) * 15);
                if (step == 0) {
                    buttonAlign.setText("Confirm First Star");
                    spinnerStar.setEnabled(true);
                    step = 1;
                } else if (step == 1) {
                   // MainActivity.t0 = tRad;
                    Double[] star1 = new Double[5];
                    star1[0] = tRad;
                    star1[1] = CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15);
                    star1[2] = CoordConvert.toRadians(MainActivity.targetRaDecl[1]);
                    star1[3] = CoordConvert.toRadians(CoordConvert.rev(MainActivity.curAltAz[0]));
                    star1[4] = CoordConvert.toRadians(MainActivity.curAltAz[1]);
                    MainActivity.allignStars.add(star1);
                    step = 2;
                    lbl1.setText("Select "+(MainActivity.allignStars.size()+1)+" Star");
                    buttonAlign.setText("Confirm "+(MainActivity.allignStars.size()+1)+" Star");
                } else {
                    Double[] star1 = new Double[5];
                    star1[0] = tRad;
                    star1[1] = CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15);
                    star1[2] = CoordConvert.toRadians(MainActivity.targetRaDecl[1]);
                    star1[3] = CoordConvert.toRadians(CoordConvert.rev(MainActivity.curAltAz[0]));
                    star1[4] = CoordConvert.toRadians(MainActivity.curAltAz[1]);
                    MainActivity.allignStars.add(star1);
                    step = 3;
                    lbl1.setText("Select "+(MainActivity.allignStars.size()+1)+" Star");
                    buttonAlign.setText("Confirm "+(MainActivity.allignStars.size()+1)+" Star");
                    buttonClear.setEnabled(true);
                }
            }
        });
        return view;
    }

    public void addItemsOnSpinner(Spinner spinner) {

// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.manual_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    public void addStarOnSpinner(Spinner spinner) {
        ArrayAdapter<Body> spinnerArrayAdapter4 = new ArrayAdapter<Body>(
                getActivity(), android.R.layout.simple_spinner_item, MainActivity.namedStars);
        spinnerArrayAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter4);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        if (parent != null) {
            Body star = (Body) spinnerStar.getSelectedItem();
            if (star != null && (star.getCatalogId() != null || star.getCommonName() != null) && step>0) {
                allignStar = star;
                MainActivity.targetRaDecl[0] = star.getRaDegreesDecimal();
                MainActivity.targetRaDecl[1] = star.getDeclDecimal();
                MainActivity.targetRaDecl = CoordConvert.convertJ2000ToJNow(MainActivity.targetRaDecl[0] , MainActivity.targetRaDecl[1]);

                double[] showAltAz = CoordConvert.calcHorizontalCoordinates(MainActivity.obsLocation[0], MainActivity.obsLocation[1], MainActivity.targetRaDecl[0],
                        MainActivity.targetRaDecl[1], 0.0);
                lbl5.setText("Az: "+ CoordConvert.decimalToDegrees(showAltAz[0]));
                lbl6.setText("Alt: "+CoordConvert.decimalToDegrees(showAltAz[1]));
                if(MainActivity.allignStars.size()>1){
//                    double[][] tMatrix = CoordConvert.allignList(MainActivity.allignStars, CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15),
//                            CoordConvert.toRadians(MainActivity.targetRaDecl[1]), MainActivity.t0);
//                    MainActivity.targetAltAz = CoordConvert.getAllignedAltLat(tMatrix, CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15),
//                            CoordConvert.toRadians(MainActivity.targetRaDecl[1]), MainActivity.t0);
                }else
                    MainActivity.targetAltAz=showAltAz;
                onResume();
            }
            if(MainActivity.targetAltAz[1]>0)
                buttonGoTo.setEnabled(true);
            MainActivity.track = false;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
