package com.example.pc.starlocator.model;

public class GeograficCoordinates {

	public static double METERS_PER_ASTRONOMICAL_UNIT = 1.4959787e+11;
	public static double METERS_PER_EARTH_EQUATORIAL_RADIUS = 6378140.0;
	public static double EARTH_RADII_PER_ASTRONOMICAL_UNIT = METERS_PER_ASTRONOMICAL_UNIT / METERS_PER_EARTH_EQUATORIAL_RADIUS;      // 23454.78

	public double longitude;
	public double latitude;
	public double radius;

	public GeograficCoordinates(double longitude, double latitude, double elevationInMetersAboveSeaLevel) {
	    this.longitude = longitude;
	    this.latitude  = latitude;
	    this.radius    = (elevationInMetersAboveSeaLevel / METERS_PER_ASTRONOMICAL_UNIT) + EARTH_RADII_PER_ASTRONOMICAL_UNIT;
	
	}
}
