package com.example.pc.starlocator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import android.content.Context;

import com.example.pc.starlocator.model.Body;

public class Parser {

	public static List<Body> doParseHip(Context context) {
		List<Body> list=new ArrayList<>();
		try {
            InputStream fstream = context.getResources().openRawResource(R.raw.hiparchos);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			String line;
			while ((line = br.readLine()) != null) {
				Body newStar = new Body();
				String[] data = line.split(",");
					newStar.setCatalogId(data[0]);
					newStar.setMagnitude(Double.parseDouble(data[1]));
					newStar.setRaDegreesDecimal(Double.parseDouble(data[2]));
					newStar.setDeclDecimal(Double.parseDouble(data[3]));
					if(data.length>4)
					newStar.setCommonName(data[4]);
					list.add(newStar);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static List<Body> doParseMessier(Context context) {
		List<Body> list=new ArrayList<>();
		try {
            InputStream fstream = context.getResources().openRawResource(R.raw.messier);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			String line;
			while ((line = br.readLine()) != null) {
				Body newStar = new Body();
				String[] data = line.split(";");
				if (data.length > 8) {

					newStar.setCatalogId(data[0]);
					newStar.setMagnitude(Double.parseDouble(data[4].replaceAll(",", ".")));
					if (data.length > 9)
						newStar.setCommonName(data[9]);
					newStar.setConstellation(data[7]);
					newStar.setType(data[1]);
					newStar.setSecType(data[8]);
					newStar.setSize(Double.parseDouble(data[5].replaceAll(",", ".")));
					newStar.setRaDegreesDecimal(15*Double.parseDouble(data[2].replaceAll(",", ".")));
					newStar.setDeclDecimal(Double.parseDouble(data[3].replaceAll(",", ".")));
					list.add(newStar);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Collections.sort(list, new Comparator<Body>(){
		     public int compare(Body o1, Body o2){
		         return Integer.parseInt(o1.getCatalogId().substring(1))- Integer.parseInt(o2.getCatalogId().substring(1));
		     }
		});
		
		return list;
	}

	public static List<Body> doParseCaldwell(Context context) {
		List<Body> list=new ArrayList<>();
		HashMap<String, String> secTypes = new HashMap<>();

		secTypes.put("BN", "Bright Nebula");
		secTypes.put("DN", "Diffuse Nebula");
		secTypes.put("EG", "Elliptical Galaxy");
		secTypes.put("FS", "Foreground Stars");
		secTypes.put("GC", "Globular Cluster");
		secTypes.put("IR", "Irregular Galaxy");
		secTypes.put("OC", "Open Cluster");
		secTypes.put("PN", "Planetary Nebula");
		secTypes.put("SG", "Spiral Galaxy");
		secTypes.put("SN", "Supernova Remnant");

		try {
            InputStream fstream = context.getResources().openRawResource(R.raw.caldwell);
		    //FileInputStream fstream = new FileInputStream("lib/files/Caldwell.csv");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			String line;
			while ((line = br.readLine()) != null) {
				Body newStar = new Body();
				String[] data = line.split(";");
				if (data.length > 11) {

					newStar.setCatalogId(data[0]);
					newStar.setCommonName(data[10]);
					newStar.setType(data[1]);
					newStar.setSecType(secTypes.get(data[11]));
					newStar.setRaDegreesDecimal(15*Double.parseDouble(data[2].replaceAll(",", ".")));
					newStar.setDeclDecimal(Double.parseDouble(data[3].replaceAll(",", ".")));
					list.add(newStar);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Collections.sort(list, new Comparator<Body>(){
		     public int compare(Body o1, Body o2){
		         return Integer.parseInt(o1.getCatalogId().substring(1))- Integer.parseInt(o2.getCatalogId().substring(1));
		     }
		});
		return list;
	}
}
