package com.example.pc.starlocator;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.io.IOException;
import java.util.ArrayList;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private FusedLocationProviderClient mFusedLocationClient;
    private static Context context;
    TextView myLabel;

    public static SettingsFragment newInstance(int page) {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.settings_fragment, container, false);
        context = getActivity().getApplicationContext();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        obtainLocation(view);
        final EditText txt1 = view.findViewById(R.id.txt1);
        final EditText txt2 = view.findViewById(R.id.txt2);
        final EditText txt3 = view.findViewById(R.id.txt3);
        final EditText txt4 = view.findViewById(R.id.txt4);
        final EditText txt5 = view.findViewById(R.id.txt5);

        txt1.setText(MainActivity.obsLocation[0] + "");
        txt2.setText(MainActivity.obsLocation[1] + "");
        txt3.setText(MainActivity.obsLocation[2] + "");
        txt4.setText(MainActivity.temperature + "");
        txt5.setText(MainActivity.pressure + "");

        final Button button = view.findViewById(R.id.button_id);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.obsLocation[0] = Double.parseDouble(txt1.getText().toString().trim());
                MainActivity.obsLocation[1] = Double.parseDouble(txt2.getText().toString().trim());
                MainActivity.obsLocation[2] = Double.parseDouble(txt3.getText().toString().trim());
                MainActivity.temperature = Integer.parseInt(txt4.getText().toString().trim());
                MainActivity.pressure = Integer.parseInt(txt5.getText().toString().trim());
            }
        });

        final ImageButton iconButton = view.findViewById(R.id.sharepic);
        iconButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                obtainLocation(view);
            }
        });

        SwitchCompat simpleSwitch = (SwitchCompat) view.findViewById(R.id.simpleSwitch);
        if (MainActivity.changedTheme)
            simpleSwitch.setChecked(true);
        else
            simpleSwitch.setChecked(false);

        simpleSwitch.setText("Night Mode");
        simpleSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (simpleSwitch.isChecked()) {
                    MainActivity.changedTheme = true;
                    ((MainActivity) getActivity()).restart();
                } else {
                    MainActivity.changedTheme = false;
                    ((MainActivity) getActivity()).restart();
                }
            }
        });
        Button openButton = (Button) view.findViewById(R.id.open);
        Button closeButton = (Button) view.findViewById(R.id.close);
        closeButton.setVisibility(View.INVISIBLE);
        myLabel = (TextView) view.findViewById(R.id.label);

        openButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openButton.setVisibility(View.INVISIBLE);
                closeButton.setVisibility(View.VISIBLE);
                myLabel.setText("Connecting");
                try {
                    myLabel.setText(((MainActivity) getActivity()).connectBlt());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openButton.setVisibility(View.VISIBLE);
                closeButton.setVisibility(View.INVISIBLE);
                myLabel.setText("Bluetooth not connected");
                ((MainActivity) getActivity()).closeBT();
            }
        });

        final Button buttonReset = view.findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.curAltAz[1] = 90;
                MainActivity.track = false;
                MainActivity.allignStars=new ArrayList<>();
                try {
                    MainActivity.message = "u" + 11650 + "\n";
                    Thread.sleep(200);
                    MainActivity.message = "e\n";
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        final Button buttonClear = view.findViewById(R.id.button_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.allignStars = new ArrayList<>();
            }
        });

        return view;
    }

    private void obtainLocation(final View view) {
        PackageManager pm = context.getPackageManager();
        int hasPerm = pm.checkPermission(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                context.getPackageName());
        if (hasPerm == 0) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                final EditText txt1 = view.findViewById(R.id.txt1);
                                final EditText txt2 = view.findViewById(R.id.txt2);
                                final EditText txt3 = view.findViewById(R.id.txt3);
                                txt1.setText(location.getLongitude() + "");
                                txt2.setText(location.getLatitude() + "");
                                txt3.setText(location.getAltitude() + "");
                            }
                        }
                    });
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }


}