package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Uranus extends Body {

	public Uranus(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}

	public Uranus(){
		super();
		setBodyData(
		        "Uranus", 
		        19.18916464,   -0.00196176,     // ja0, jac = semi-major axis (AU), rate (AU/century)
		         0.04725744,   -0.00004397,     // je0, jec = eccentricity (1), (1/century)
		         0.77263783,   -0.00242939,     // jI0, jIc = inclination (deg), (deg/century)
		       313.23810451,  428.48202785,     // jL0, jLc = mean longitude (deg), (deg/century)
		       170.95427630,    0.40805281,     // ju0, juc = longitude of perihelion (deg), (deg/century)
		        74.01692503,    0.04240589,     // jQ0, jQc = longitude of ascending node (deg), (deg/century)
		        -7.15, 0.001, 0, 0      // visual magnitude elements
		    );
	}
	@Override
	public CartesianCoordinates perturb(double xh, double yh, double zh, double d) {
		SphericalCoordinates ecliptic = AstrUtil.EclipticLatLon(xh, yh, zh);
		double lonecl = ecliptic.longitude;
		double latecl = ecliptic.latitude;

		double r = Math.sqrt(xh * xh + yh * yh + zh * zh);
		double Mj = AstrUtil.jupiterMeanAnomaly(d);
		double Ms = AstrUtil.saturnMeanAnomaly(d);
		double Mu = getMeanAnomaly(d);

		lonecl += +0.040 * AstrUtil.SinDeg(Ms - 2 * Mu + 6) + 0.035 * AstrUtil.SinDeg(Ms - 3 * Mu + 33)
				- 0.015 * AstrUtil.SinDeg(Mj - Mu + 20);

		double coslon = AstrUtil.CosDeg(lonecl);
		double sinlon = AstrUtil.SinDeg(lonecl);
		double coslat = AstrUtil.CosDeg(latecl);
		double sinlat = AstrUtil.SinDeg(latecl);

		double xp = r * coslon * coslat;
		double yp = r * sinlon * coslat;
		double zp = r * sinlat;

		return new CartesianCoordinates(xp, yp, zp);
	}

}
