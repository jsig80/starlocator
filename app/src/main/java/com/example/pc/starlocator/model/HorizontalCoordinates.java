package com.example.pc.starlocator.model;

public class HorizontalCoordinates {

	private double altitude;
	private double azimuth;
	
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
	public double getAzimuth() {
		return azimuth;
	}
	public void setAzimuth(double azimuth) {
		this.azimuth = azimuth;
	}
}
