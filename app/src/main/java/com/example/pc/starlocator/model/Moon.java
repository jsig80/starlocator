package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Moon extends Body {

	public double RadiusInMeters;
	
	public Moon(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}

	@Override
	public void calcPosition(GeograficCoordinates location,Earth earth) {
		double day = AstrUtil.DayValue();
		geocentricCoordinates=calcGeocentricCoordinates(day, eclipticCartesianCoordinates,earth);		
		geocentricCoordinates= perturb (geocentricCoordinates.x,
				geocentricCoordinates.y, geocentricCoordinates.z, day);		

        CartesianCoordinates mc = geocentricCoordinates;
        CartesianCoordinates ec = earth.getEclipticCartesianCoordinates();
		eclipticCartesianCoordinates=ec.Add (mc);
		//eclipticCartesianCoordinates=calcEclipticCartesianCoordinates(day);
		eclipticAngularCoordinates=calcEclipticAngularCoordinates(day, eclipticCartesianCoordinates);
		eqCoords=calcEqCoords(day, location);
		horCoords=calcHorizontalCoordinates(day, location);
		this.setMagnitude(VisualMagnitude(AstrUtil.DayValue()));
	}
	
	@Override
	public CartesianCoordinates calcGeocentricCoordinates(double day, CartesianCoordinates ccoords,Earth earth) 
	{
		return super.calcEclipticCartesianCoordinates(day);
	}
	
	@Override
	public CartesianCoordinates perturb(double xh, double yh, double zh, double d) {
		SphericalCoordinates ecliptic = AstrUtil.EclipticLatLon(xh, yh, zh);
		double lonecl = ecliptic.longitude;
		double latecl = ecliptic.latitude;

		double r = Math.sqrt(xh * xh + yh * yh + zh * zh);
		double Ms = AstrUtil.MeanAnomalyOfSun(d); // mean anomaly of Sun
		double ws = AstrUtil.SunArgumentOfPerihelion(d); // Sun's argument of
															// perihelion
		double Ls = Ms + ws;
		double Mm = getMeanAnomaly(d); // Moon's mean anomaly
		double Nm = this.getN0() + (d * this.getNc()); // longitude of Moon's node
		double wm = this.getW0() + (d * this.getWc()); // Moon's argument of perihelion
		double Lm = Mm + wm + Nm; // Mean longitude of the Moon

		double D = Lm - Ls; // mean elongation of the Moon
		double F = Lm - Nm; // argument of latitude for the Moon

		lonecl += -1.274 * AstrUtil.SinDeg(Mm - 2 * D) + // the Evection
				0.658 * AstrUtil.SinDeg(2 * D) - // the doubleiation
				0.186 * AstrUtil.SinDeg(Ms) - // the Yearly Equation
				0.059 * AstrUtil.SinDeg(2 * Mm - 2 * D) - 0.057 * AstrUtil.SinDeg(Mm - 2 * D + Ms)
				+ 0.053 * AstrUtil.SinDeg(Mm + 2 * D) + 0.046 * AstrUtil.SinDeg(2 * D - Ms)
				+ 0.041 * AstrUtil.SinDeg(Mm - Ms) - 0.035 * AstrUtil.SinDeg(D) - 
				0.031 * AstrUtil.SinDeg(Mm + Ms) - 0.015 * AstrUtil.SinDeg(2 * F - 2 * D)
				+ 0.011 * AstrUtil.SinDeg(Mm - 4 * D);

		latecl+=  -0.173 * AstrUtil.SinDeg(F - 2*D)       -
	             0.055 * AstrUtil.SinDeg(Mm - F - 2*D)  -
	             0.046 * AstrUtil.SinDeg(Mm + F - 2*D)  +
	             0.033 * AstrUtil.SinDeg(F + 2*D)       +
	             0.017 * AstrUtil.SinDeg(2*Mm + F)
	        ;
				
		 double deltaRadius =
		            -0.58 * AstrUtil.CosDeg (Mm - 2*D)   -
		             0.46 * AstrUtil.CosDeg (2*D)
		        ;
		 r += deltaRadius / AstrUtil.EARTH_RADII_PER_ASTRONOMICAL_UNIT;
		 
		double coslon = AstrUtil.CosDeg(lonecl);
		double sinlon = AstrUtil.SinDeg(lonecl);
		double coslat = AstrUtil.CosDeg(latecl);
		double sinlat = AstrUtil.SinDeg(latecl);

		double xp = r * coslon * coslat;
		double yp = r * sinlon * coslat;
		double zp = r * sinlat;

		return new CartesianCoordinates(xp, yp, zp);
	}

	public double getRadiusInMeters() {
		return RadiusInMeters;
	}

	public void setRadiusInMeters(double radiusInMeters) {
		RadiusInMeters = radiusInMeters;
	}
	
}
