package com.example.pc.starlocator.model;

public class SphericalCoordinates {

	public double longitude;
	public double latitude;
	public double radius;

	public SphericalCoordinates (double longitude, double latitude, double radius)
	{
	    this.longitude = longitude;
	    this.latitude  = latitude;
	    this.radius    = radius;
	}
}
