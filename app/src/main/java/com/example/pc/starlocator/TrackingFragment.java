package com.example.pc.starlocator;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.pc.starlocator.model.Body;
import com.example.pc.starlocator.model.GeograficCoordinates;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrackingFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    Spinner spinner1, spinner2;
    TextView lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8;
    Button buttonGoto, buttonTrack, button_add, button_up_t, button_left_t, button_right_t, button_down_t, button_up,
            button_left, button_right, button_down;
    AutoCompleteTextView actv;
    double[] showAltAz = new double[2];
    double[][] tMatrix = new double[3][3];
    double dx=0;
    double dy=0;
    double t0;//time of first allignment star in radians
    Handler h = new Handler();
    int delay = 4000;
    Runnable runnable;
    static String selectedCat;
    static Body selectedBody;
    private static DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public void onResume() {
        //start handler as activity become visible

        h.postDelayed(new Runnable() {
            public void run() {
                if (MainActivity.allignStars.size() > 0)
                    lbl8.setText(MainActivity.allignStars.size() + " Align Points");
                else
                    lbl8.setText("No Align Points");
                if (selectedCat != null && selectedBody != null) {
                    if (selectedCat.equals("Planets")) {
                        GeograficCoordinates location = new GeograficCoordinates(MainActivity.obsLocation[0], MainActivity.obsLocation[1],
                                MainActivity.obsLocation[2]);
                        MainActivity.earth.calcPosition(location, null);
                        selectedBody.calcPosition(location, MainActivity.earth);
                        lbl2.setText("RA: " + CoordConvert.decimalToHours(selectedBody.getEqCoords().longitude));
                        lbl3.setText("Decl: " + CoordConvert.decimalToDegrees(selectedBody.getEqCoords().latitude));
                        showAltAz[0] = selectedBody.getHorCoords().getAzimuth();
                        showAltAz[1] = selectedBody.getHorCoords().getAltitude();
                        if (tMatrix != null)
                            MainActivity.targetAltAz = CoordConvert.getAllignedAltLat(tMatrix, CoordConvert.toRadians(selectedBody.getEqCoords().longitude * 15),
                                    CoordConvert.toRadians(selectedBody.getEqCoords().latitude), t0);
                        else
                            MainActivity.targetAltAz = showAltAz;
                        lbl6.setText("Magnitude: " + df.format(selectedBody.getMagnitude()) + "");
                    } else {

                        showAltAz = CoordConvert.calcHorizontalCoordinates(MainActivity.obsLocation[0], MainActivity.obsLocation[1], MainActivity.targetRaDecl[0],
                                MainActivity.targetRaDecl[1], 0.0);
                        if (tMatrix != null)
                            MainActivity.targetAltAz = CoordConvert.getAllignedAltLat(tMatrix, CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15),
                                    CoordConvert.toRadians(MainActivity.targetRaDecl[1]), t0);
                        else
                            MainActivity.targetAltAz = showAltAz;
                    }

                    lbl4.setText("Az: " + CoordConvert.decimalToDegrees(showAltAz[0]));
                    lbl5.setText("Alt: " + CoordConvert.decimalToDegrees(showAltAz[1]));

                    if (showAltAz[1] > 0) {
                        buttonGoto.setEnabled(true);
                        buttonTrack.setEnabled(true);
                        if (MainActivity.track) {
                            MainActivity.targetAltAz[0]+=dx;
                            MainActivity.targetAltAz[1]+=dy;
                            MainActivity.track();
                        }
                    } else {
                        buttonGoto.setEnabled(false);
                        buttonTrack.setEnabled(false);
                    }
                }
                runnable = this;

                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    public void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    public TrackingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tracking, container, false);

        lbl2 = view.findViewById(R.id.lbl2);
        lbl3 = view.findViewById(R.id.lbl3);
        lbl4 = view.findViewById(R.id.lbl4);
        lbl5 = view.findViewById(R.id.lbl5);
        lbl6 = view.findViewById(R.id.lbl6);
        lbl8 = view.findViewById(R.id.lbl8);
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
        addItemsOnSpinner(spinner1);
        spinner1.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);
        buttonGoto = view.findViewById(R.id.button_goto);
        buttonGoto.setEnabled(false);
        buttonTrack = view.findViewById(R.id.button_track);
        buttonTrack.setEnabled(false);
        spinner2.setVisibility(View.INVISIBLE);
        actv = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        actv.setVisibility(View.INVISIBLE);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                Body star = (Body) actv.getAdapter().getItem(pos);
                CoordConvert.findJd();

                if (star != null && (star.getCatalogId() != null || star.getCommonName() != null)) {
                    selectedBody = star;
                    MainActivity.targetRaDecl[0] = star.getRaDegreesDecimal();
                    MainActivity.targetRaDecl[1] = star.getDeclDecimal();
                    MainActivity.targetRaDecl = CoordConvert.convertJ2000ToJNow(MainActivity.targetRaDecl[0], MainActivity.targetRaDecl[1]);
                    lbl2.setText("RA: " + CoordConvert.decimalToHours(MainActivity.targetRaDecl[0]));
                    lbl3.setText("Decl: " + CoordConvert.decimalToDegrees(MainActivity.targetRaDecl[1]));
                    lbl6.setText("Magnitude: " + star.getMagnitude() + "");
                    if (MainActivity.allignStars.size() > 1) {
                        tMatrix = CoordConvert.allignList(MainActivity.allignStars, CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15),
                                CoordConvert.toRadians(MainActivity.targetRaDecl[1]), t0);
                    } else
                        tMatrix = null;
                    //MainActivity.calculate = true;
                    onResume();
                }
                buttonGoto.setEnabled(false);
                buttonTrack.setEnabled(false);
                MainActivity.track = false;
                buttonTrack.setText("start tracking");
                button_up_t.setVisibility(View.INVISIBLE);
                button_left_t.setVisibility(View.INVISIBLE);
                button_right_t.setVisibility(View.INVISIBLE);
                button_down_t.setVisibility(View.INVISIBLE);
                dx=0;
                dy=0;
                button_up.setVisibility(View.VISIBLE);
                button_left.setVisibility(View.VISIBLE);
                button_right.setVisibility(View.VISIBLE);
                button_down.setVisibility(View.VISIBLE);
            }
        });
        actv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredText = actv.getText().toString();
                refreshList(enteredText);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Spinner spinner = (Spinner) view.findViewById(R.id.spinnerMin);
        addMinutesOnSpinner(spinner);

        button_up_t = view.findViewById(R.id.button_up_t);
        button_up_t.setVisibility(View.INVISIBLE);
        button_up_t.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dy+=0.1;
            }
        });

        button_left_t = view.findViewById(R.id.button_left_t);
        button_left_t.setVisibility(View.INVISIBLE);
        button_left_t.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dx+=-0.1;
            }
        });

        button_right_t = view.findViewById(R.id.button_right_t);
        button_right_t.setVisibility(View.INVISIBLE);
        button_right_t.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dx+=0.1;
            }
        });

        button_down_t = view.findViewById(R.id.button_down_t);
        button_down_t.setVisibility(View.INVISIBLE);
        button_down_t.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dy+=-0.1;
            }
        });

        button_up = view.findViewById(R.id.button_up);
        button_up.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[1] += degrees;
                MainActivity.track = false;
                buttonTrack.setText("start tracking");
                MainActivity.track();
            }
        });

        button_left = view.findViewById(R.id.button_left);
        button_left.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[0] -= degrees;
                MainActivity.track = false;
                buttonTrack.setText("start tracking");
                MainActivity.track();
            }
        });

        button_right = view.findViewById(R.id.button_right);
        button_right.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[0] += degrees;
                MainActivity.track = false;
                buttonTrack.setText("start tracking");
                MainActivity.track();
            }
        });

        button_down = view.findViewById(R.id.button_down);
        button_down.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int minutes = Integer.parseInt(spinner.getSelectedItem().toString());
                MainActivity.targetAltAz[0] = MainActivity.curAltAz[0];
                MainActivity.targetAltAz[1] = MainActivity.curAltAz[1];
                double degrees = minutes / 60;
                double min = (float) (minutes % 60) / 60;
                degrees += min;
                MainActivity.targetAltAz[1] -= degrees;
                MainActivity.track = false;
                buttonTrack.setText("start tracking");
                MainActivity.track();
            }
        });

        button_add = view.findViewById(R.id.button_add);
        button_add.setEnabled(false);
        button_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Date now = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.setTimeZone(TimeZone.getTimeZone("UTC"));
                float ut = cal.get(Calendar.HOUR_OF_DAY);
                float min = cal.get(Calendar.MINUTE);
                float sec = cal.get(Calendar.SECOND);
                float dec = ((min * 60) + sec) / 3600;
                ut += dec;
                if (MainActivity.allignStars.size() == 0)
                    t0 = CoordConvert.toRadians((ut) * 15);
                Double[] star1 = new Double[5];
                star1[0] = CoordConvert.toRadians((ut) * 15);
                star1[1] = CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15);
                star1[2] = CoordConvert.toRadians(MainActivity.targetRaDecl[1]);
                star1[3] = CoordConvert.toRadians(CoordConvert.rev(MainActivity.curAltAz[0]));
                star1[4] = CoordConvert.toRadians(MainActivity.curAltAz[1]);
                MainActivity.allignStars.add(star1);
                lbl8.setText(MainActivity.allignStars.size() + " Align Points");
            }
        });

        buttonGoto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.track();
            }
        });
        buttonTrack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MainActivity.track) {
                    MainActivity.track = false;
                    buttonTrack.setText("start tracking");
                    button_up_t.setVisibility(View.INVISIBLE);
                    button_left_t.setVisibility(View.INVISIBLE);
                    button_right_t.setVisibility(View.INVISIBLE);
                    button_down_t.setVisibility(View.INVISIBLE);
                    dx=0;
                    dy=0;
                    button_up.setVisibility(View.VISIBLE);
                    button_left.setVisibility(View.VISIBLE);
                    button_right.setVisibility(View.VISIBLE);
                    button_down.setVisibility(View.VISIBLE);
                } else {
                    MainActivity.track = true;
                    MainActivity.curAltAz[0] = MainActivity.targetAltAz[0];
                    MainActivity.curAltAz[1] = MainActivity.targetAltAz[1];
                    buttonTrack.setText("stop tracking");
                    button_up_t.setVisibility(View.VISIBLE);
                    button_left_t.setVisibility(View.VISIBLE);
                    button_right_t.setVisibility(View.VISIBLE);
                    button_down_t.setVisibility(View.VISIBLE);
                    dx=0;
                    dy=0;
                    button_up.setVisibility(View.INVISIBLE);
                    button_left.setVisibility(View.INVISIBLE);
                    button_right.setVisibility(View.INVISIBLE);
                    button_down.setVisibility(View.INVISIBLE);
                }
            }
        });
        return view;
    }

    public void refreshList(String text) {
        List<Body> hipStars = MainActivity.queryList(text);
        ArrayAdapter<Body> spinnerArrayAdapter6 = new ArrayAdapter<Body>(
                getActivity(), android.R.layout.simple_spinner_item, hipStars);
        spinnerArrayAdapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actv.setAdapter(spinnerArrayAdapter6);
    }

    public void addMinutesOnSpinner(Spinner spinner) {

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.manual_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    public void addItemsOnSpinner(Spinner spinner) {
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.list_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        if (parent != null) {
            int spinid = parent.getId();
            if (spinid == R.id.spinner1) {
                selectedCat = parent.getItemAtPosition(pos).toString();
                switch (parent.getItemAtPosition(pos).toString()) {
                    case "Messier":
                        List<Body> listM = Parser.doParseMessier(getActivity().getApplicationContext());
                        ArrayAdapter<Body> spinnerArrayAdapter1 = new ArrayAdapter<Body>(
                                getActivity(), android.R.layout.simple_spinner_item, listM);
                        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner2.setAdapter(spinnerArrayAdapter1);
                        spinner2.setVisibility(View.VISIBLE);
                        actv.setVisibility(View.INVISIBLE);
                        button_add.setEnabled(false);
                        break;
                    case "Planets":
                        MainActivity.planets = new ArrayList<Body>();
                        Astronomy astr = new Astronomy();
                        astr.createAstronomyClass(MainActivity.planets);
                        ArrayAdapter<Body> spinnerArrayAdapter2 = new ArrayAdapter<Body>(
                                getActivity(), android.R.layout.simple_spinner_item, MainActivity.planets);
                        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner2.setAdapter(spinnerArrayAdapter2);
                        spinner2.setVisibility(View.VISIBLE);
                        actv.setVisibility(View.INVISIBLE);
                        button_add.setEnabled(false);
                        break;
                    case "Caldwell":
                        List<Body> listC = Parser.doParseCaldwell(getActivity().getApplicationContext());
                        ArrayAdapter<Body> spinnerArrayAdapter3 = new ArrayAdapter<Body>(
                                getActivity(), android.R.layout.simple_spinner_item, listC);
                        spinnerArrayAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner2.setAdapter(spinnerArrayAdapter3);
                        spinner2.setVisibility(View.VISIBLE);
                        actv.setVisibility(View.INVISIBLE);
                        button_add.setEnabled(false);
                        break;
                    case "Named Stars":
                        if (MainActivity.namedStars != null && MainActivity.namedStars.size() > 100) {
                            ArrayAdapter<Body> spinnerArrayAdapter4 = new ArrayAdapter<Body>(
                                    getActivity(), android.R.layout.simple_spinner_item, MainActivity.namedStars);
                            spinnerArrayAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner2.setAdapter(spinnerArrayAdapter4);
                            spinner2.setVisibility(View.VISIBLE);
                            actv.setVisibility(View.INVISIBLE);
                            button_add.setEnabled(true);
                        }
                        break;
                    case "Hipparchos Cat":
                        actv.setVisibility(View.VISIBLE);
                        spinner2.setVisibility(View.INVISIBLE);
                        button_add.setEnabled(true);
                        break;
                }
            } else {
                Body star = (Body) spinner2.getSelectedItem();
                CoordConvert.findJd();

                if (star != null && (star.getCatalogId() != null || star.getCommonName() != null)) {
                    selectedBody = star;
                    if (!spinner1.getSelectedItem().equals("Planets")) {
                        MainActivity.targetRaDecl[0] = star.getRaDegreesDecimal();
                        MainActivity.targetRaDecl[1] = star.getDeclDecimal();
                        MainActivity.targetRaDecl = CoordConvert.convertJ2000ToJNow(MainActivity.targetRaDecl[0], MainActivity.targetRaDecl[1]);
                        lbl2.setText("RA: " + CoordConvert.decimalToHours(MainActivity.targetRaDecl[0]));
                        lbl3.setText("Decl: " + CoordConvert.decimalToDegrees(MainActivity.targetRaDecl[1]));
                        lbl6.setText("Magnitude: " + star.getMagnitude() + "");
                    }
                    if (MainActivity.allignStars.size() > 1) {
                        tMatrix = CoordConvert.allignList(MainActivity.allignStars, CoordConvert.toRadians(MainActivity.targetRaDecl[0] * 15),
                                CoordConvert.toRadians(MainActivity.targetRaDecl[1]), t0);
                    } else
                        tMatrix = null;
                    //MainActivity.calculate = true;
                    onResume();
                }
                buttonGoto.setEnabled(false);
                buttonTrack.setEnabled(false);
                MainActivity.track = false;
                buttonTrack.setText("start tracking");
                button_up_t.setVisibility(View.INVISIBLE);
                button_left_t.setVisibility(View.INVISIBLE);
                button_right_t.setVisibility(View.INVISIBLE);
                button_down_t.setVisibility(View.INVISIBLE);
                dx=0;
                dy=0;
                button_up.setVisibility(View.VISIBLE);
                button_left.setVisibility(View.VISIBLE);
                button_right.setVisibility(View.VISIBLE);
                button_down.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
