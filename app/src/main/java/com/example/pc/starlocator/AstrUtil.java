package com.example.pc.starlocator;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.example.pc.starlocator.model.Body;
import com.example.pc.starlocator.model.CartesianCoordinates;
import com.example.pc.starlocator.model.SphericalCoordinates;

public class AstrUtil {
//----------------------------------------------------------------------------------------------
// Numeric constants...

	public static double METERS_PER_ASTRONOMICAL_UNIT = 1.4959787e+11;
	public static double METERS_PER_EARTH_EQUATORIAL_RADIUS = 6378140.0;
	public static double EARTH_RADII_PER_ASTRONOMICAL_UNIT = METERS_PER_ASTRONOMICAL_UNIT / METERS_PER_EARTH_EQUATORIAL_RADIUS;      // 23454.78

//----------------------------------------------------------------------------------------------
// Custom trigonometry and math functions.
// We do most of our math in angular degrees and sideral hours, instead of radians.

	public static double DEG_FROM_RAD = 180.0 / Math.PI;
	public static double RAD_FROM_DEG = Math.PI / 180.0;
	public static double HOURS_FROM_RAD = 12.0 / Math.PI;
	public static double RAD_FROM_HOURS = Math.PI / 12.0;

	public static double CosDeg(double degrees)
    {
        return Math.cos (RAD_FROM_DEG * degrees);
    }

	public static double SinDeg(double degrees)
    {
        return Math.sin (RAD_FROM_DEG * degrees);
    }
    
	public static double TanDeg(double degrees) 
    {
        return Math.tan (RAD_FROM_DEG * degrees);
    }
    
	public static double CosHour(double hours)
    {
        return Math.cos (RAD_FROM_HOURS * hours);
    }

	public static double SinHour (double hours)
    {
        return Math.sin (RAD_FROM_HOURS * hours);
    }
    
	public static double AtanDeg2   (double y, double x)
    {
        return DEG_FROM_RAD * Math.atan2 (y, x);
    }
    
    public static double FixHours   (double hours)
    {
        return FixCycle (hours, 24.0);
    }
    
    public static double FixDegrees   (double degrees)
    {
        return FixCycle (degrees, 360.0);
    }
    
    public static double FixCycle   (double angle, double cycle)
    {
        double fraction = angle / cycle;
        return cycle * (fraction - Math.floor (fraction));
    }
    
    public static SphericalCoordinates Polar   (double x, double y,double z)
    {
        double rho = (x * x) + (y * y);
        double radius = Math.sqrt (rho + (z * z));
        double phi = AtanDeg2 (y, x);
        if (phi < 0) {
            phi += 360.0;
        }
        rho = Math.sqrt (rho);
        double theta = AtanDeg2 (z, rho);
        return new SphericalCoordinates (phi, theta, radius);
    }
    
   
    public static double[] DMS(double x)
    {
        double[] a = new double[4];
        
        a[0] = 1;
        if (x < 0) {
            x = -x;
            a[0] = -1;
        }
        
        a[1] = Math.floor (x);
        x = 60.0 * (x - a[1]);
        a[2] = Math.floor (x);
        x = 60.0 * (x - a[2]);
        a[3] = Math.round (10.0 * x) / 10.0;   // Round to the nearest tenth of an arcsecond.
        
        if (a[3] == 60) {
        	a[3] = 0;
            if (++a[2] == 60) {
                a[2] = 0;
                ++a[1];
            }
        }
        
        return a;
    }
    
    public static double[] DMM   (double x)
    {
    	double[] a = new double[4];
    	
    	a[0] = 1;
        if (x < 0) {
            x = -x;
            a[0] = -1;
        }
        
        a[1] = Math.floor (x);
        x = 60.0 * (x - a[1]);
        a[2] = Math.round (100.0 * x) / 100.0;     // Round to nearest hundredth of an arcminute.
        a[3] = 0.0;        // Fill in just for consistency with Angle.DMS
        
        if (a[2] >= 60.0) {
            a[2] -= 60.0;
            ++a[1];
        }
        
        return a;
    }
    
    public static double SafeArcSinInDegrees   (double z)
    {
        double abs = Math.abs (z);
        if (abs > 1.0) {
            // Guard against blowing up due to slight roundoff errors in Math.Asin ...
            if (abs > 1.00000001) {
                // "Invalid argument to SafeArcSinInDegrees";
            	return 0;
            } else if (z < -1.0) {
                return -90.0;
            } else {
                return +90.0;
            }
        } else {
            return DEG_FROM_RAD * Math.asin(z);
        }
    }

  

    public static double EccentricAnomaly (double e,double M)
    {
        double E = M + (e * SinDeg(M) * (1.0 + (e * CosDeg(M))));
        while(true) {
            double F = E - (E - (DEG_FROM_RAD * e * SinDeg (E)) - M) / (1 - e * CosDeg (E));
            double error =Math.abs (F - E);
            E = F;
            if (error < 1.0e-8) {
                break;  // the angle is good enough now for our purposes
            }
        }
        return E;
    }
    
    public static double MeanAnomalyOfSun (double d)
{
    return 356.0470 + (0.9856002585 * d);
}

    public static double SunArgumentOfPerihelion (double d)
{
    return 282.9404 + (4.70935e-5 * d);
}

    public static double MeanLongitudeOfSun (double d)
{
    return MeanAnomalyOfSun(d) + SunArgumentOfPerihelion(d);
}
    
 public static boolean AnglesInOrder (double a,double b,double c)
{
    a = FixDegrees (a);
    b = FixDegrees (b);
    c = FixDegrees (c);

    double MARGIN = 45.0;
    if (c < a) {
        double swap = a;
        a = c;
        c = swap;
    }

    if (a <= MARGIN && c >= 360.0 - MARGIN) {
        // this is the wraparound case
        return ((b >= 0.0) && (b <= a)) || ((b >= c) && (b <= 360.0));
    } else if (c - a <= 2 * MARGIN) {
        return (a <= b) && (b <= c);
    } else {
    	return false;
        // "AnglesInOrder continuity failure!";
    }
}

 public static double DayValue()
 {
     // http://www.elated.com/articles/working-with-dates/
     // Return number of days since 0/Jan/2000 00:00 UTC...
	 Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar epoch = Calendar.getInstance();
		epoch.setTimeZone(TimeZone.getTimeZone("UTC"));
		epoch.clear();
		epoch.set(2000, 0, 1);
		return 1.0 + (cal.getTimeInMillis() - epoch.getTimeInMillis()) / (3600.0 * 24.0 * 1000.0);
 }
 
 public static double DaysSinceJ2000  (double day)
 {
     return day - 1.5;
 }
 
 public static double CenturiesSinceJ2000  (double day)
 {
     return DaysSinceJ2000(day) / 36525.0;
 }
 
// public static double CurrentDayValue  (utc)
// {
//     return this.DayValue(new Date());
// }
 
// public static double DayValueToDate   (double day)
// {
//     double date = new Date();
//     double M = 3600.0 * 24.0 * 1000.0;
//     double T0 = Date.UTC (2000, 0, 1);
//     date.setTime (M*(day - 1.0) + T0);
//     return date;
// }
 
 public static SphericalCoordinates EclipticLatLon   (double x, double y, double z)
 {
	 SphericalCoordinates sc=new SphericalCoordinates(AtanDeg2(y, x),AtanDeg2(z, Math.sqrt(x*x + y*y)),0);
	 
     return sc;
 }
// 
// public static double NextRiseTime   (Body body, double day, location)
// {
//     return Astronomy_FindNextTransition (body, day, day + 1.1, location, Astronomy_RiseCondition, 3);
// }
// 
// public static double NextSetTime   (body, day, location)
// {
//     return Astronomy_FindNextTransition (body, day, day + 1.1, location, Astronomy_SetCondition, 3);
// }
// 
// public static double NextCulmTime   (body, day, location)
// {
//     return Astronomy_FindNextTransition (body, day, day + 1.1, location, Astronomy_CulminateCondition, 6);
// }
//
// public static double NextNewMoon   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Moon, day, day + numDays, null, Astronomy_RelativeLongitudeCondition, numIntervals, 0.0);
// }
// 
// public static double NextMoonFirstQuarter   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Moon, day, day + numDays, null, Astronomy_RelativeLongitudeCondition, numIntervals, 90.0);
// }
//
// public static double NextFullMoon   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Moon, day, day + numDays, null, Astronomy_RelativeLongitudeCondition, numIntervals, 180.0);
// }
//
// public static double NextMoonThirdQuarter   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Moon, day, day + numDays, null, Astronomy_RelativeLongitudeCondition, numIntervals, 270.0);
// }
// 
// public static double NextOpposition   (body, day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (body, day, day + numDays, null, Astronomy_RelativeLongitudeCondition, numIntervals, 180.0);
// }
// 
// public static double NextConjunction   (body, day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (body, day, day + numDays, null, Astronomy_RelativeLongitudeCondition, numIntervals, 0.0);
// }
// 
// public static double NextMaxSunAngle   (body, day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (body, day, day + numDays, null, Astronomy_MaxSunAngleCondition, numIntervals);
// }
// 
// public static double NextVernalEquinox   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Sun, day, day + numDays, null, Astronomy_VernalEquinoxCondition, numIntervals);
// }
//
// public static double NextAutumnalEquinox   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Sun, day, day + numDays, null, Astronomy_AutumnalEquinoxCondition, numIntervals);
// }
//
// public static double NextNorthernSolstice   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Sun, day, day + numDays, null, Astronomy_NorthernSolsticeCondition, numIntervals);
// }
//
// public static double NextSouthernSolstice   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (this.Sun, day, day + numDays, null, Astronomy_SouthernSolsticeCondition, numIntervals);
// }
// 
// public static double NextPeakVisualMagnitude   (body, day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (body, day, day + numDays, null, Astronomy_PeakVisualMagnitudeCondition, numIntervals);
// }
//
// public static double NextMinAngleWithOtherBody   (body, day, numDays, numIntervals, otherBody)
// {
//     return Astronomy_FindNextTransition (body, day, day + numDays, null, Astronomy_MinAngleWithOtherBodyCondition, numIntervals, otherBody);
// }
//
// public static double NextMoonPerigee   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (null, day, day + numDays, null, Astronomy_MoonPerigee, numIntervals, null);
// }
//
// public static double NextMoonApogee   (day, numDays, numIntervals)
// {
//     return Astronomy_FindNextTransition (null, day, day + numDays, null, Astronomy_MoonApogee, numIntervals, null);
// }
// 
 public static double SunEarthPhaseAngle   (Body body, double day)
 {
     if (body.getCommonName().equals("Sun")) {
         // "Cannot calculate SunEarthPhaseAngle of the Sun";
    	 return 0;
     } else {
    	 CartesianCoordinates h = body.getEclipticCartesianCoordinates ();        // a.k.a. Heliocentric Coordinates
         CartesianCoordinates g = body.getGeocentricCoordinates ();
         
         // We take advantage of the fact that when two lines cross, opposite angles are equal.
         // In other words, we want the angle between (-h) and (-g), which is the same as the angle between h and g,
         // because (-h) dot (-g) = h dot g :
         // ((-hx)*(-gx) = hx*gx, etc.

         return AngleBetweenVectorsInDegrees (h, g);
     }
 }
// 
// public static double AngleWithSunInDegrees   (body, day)
// {
//     if (body.Name == "Sun") {
//         return 0.0;
//     } else {
//         double s = Astronomy.Sun.GeocentricCoordinates (day);
//         double b = body.GeocentricCoordinates (day);
//         return AngleBetweenVectorsInDegrees (s, b);
//     }
// }
//
// public static double AngleBetweenBodiesInDegrees   (body1, body2, day)
// {
//     double a = body1.GeocentricCoordinates (day);
//     double b = body2.GeocentricCoordinates (day);
//     return AngleBetweenVectorsInDegrees (a, b);
// }
// 
// public static double RelativeLongitude   (body, day)
// {
//     // Returns the relative longitude between the body and the Sun as seen from Earth's center.
//     double bc = body.GeocentricCoordinates (day);
//     double sc = this.Sun.GeocentricCoordinates (day);
//     
//     double blon = Angle.AtanDeg2 (bc.y, bc.x);
//     double slon = Angle.AtanDeg2 (sc.y, sc.x);
//     double rlon = Angle.FixDegrees (blon - slon);
//     return rlon;
// }
// 
// public static double PrecessionRotationMatrix   (EPOCH1, EPOCH2)
// {
//     double CDR = Math.PI / 180.0;
//     double CSR = CDR / 3600.0;
//     double T = 0.001 * (EPOCH2 - EPOCH1);
//     double ST = 0.001 * (EPOCH1 - 1900.0);
//     double A = CSR * T * (23042.53 + ST * (139.75 + 0.06 * ST) + T * (30.23 - 0.27 * ST + 18.0 * T));
//     double B = CSR * T * T * (79.27 + 0.66 * ST + 0.32 * T) + A;
//     double C = CSR * T * (20046.85 - ST * (85.33 + 0.37 * ST) + T * (-42.67 - 0.37 * ST - 41.8 * T));
//     double SINA = Math.sin (A);
//     double SINB = Math.sin (B);
//     double SINC = Math.sin (C);
//     double COSA = Math.cos (A);
//     double COSB = Math.cos (B);
//     double COSC = Math.cos (C);
//
//     double r = [[], [], []];
//     r[0][0] = COSA * COSB * COSC - SINA * SINB;
//     r[0][1] = -COSA * SINB - SINA * COSB * COSC;
//     r[0][2] = -COSB * SINC;
//     r[1][0] = SINA * COSB + COSA * SINB * COSC;
//     r[1][1] = COSA * COSB - SINA * SINB * COSC;
//     r[1][2] = -SINB * SINC;
//     r[2][0] = COSA * SINC;
//     r[2][1] = -SINA * SINC;
//     r[2][2] = COSC;
//
//     return r;
// }
// 
// // We are taking a short-cut for computational efficiency:
// // We calculate the rotation matrix for the current date a single time,
// // then keep using it.  This will be plenty accurate for anything
// // less than a decade before or after the current date.
// //
// public static double ConstellationRotationMatrix = this.PrecessionRotationMatrix (new Date().getFullYear(), 1875);
// 
// /*
//     The PrecessEquatorialCoordinates() function was translated from C to C# by Don Cross, 
//     then to Javascript by Don Cross, based on
//     code with original comments thus:
//
//     This program is a translation with a few adaptations of the 
//     Fortran program.f, made by FO @ CDS  (francois@simbad.u-strasbg.fr)  
//     in November 1996.
// */
// public static double PrecessEquatorialCoordinates   (eq, matrix)
// {
//     // Don says: in the original C code, the RA1 and DEC1 passed in were always in radians.
//     // I want to pass in hours and degrees, respectively, so convert to radians here...
//     double RA1  = Angle.RAD_FROM_HOURS * eq.longitude;
//     double DEC1 = Angle.RAD_FROM_DEG   * eq.latitude;
//
//     /* Compute input direction cosines */
//     double A = Math.cos (DEC1);
//
//     double x1 = [
//         A * Math.cos (RA1),
//         A * Math.sin (RA1),
//         Math.sin (DEC1)
//     ];
//
//     /* Perform the rotation to get the direction cosines at epoch2 */
//     double x2 = [ 0.0, 0.0, 0.0 ];
//     for (double i = 0; i < 3; i++) {
//         for (double j = 0; j < 3; j++) {
//             x2[i] += matrix[i][j] * x1[j];
//         }
//     }
//
//     // Don says: for my purposes, I always want RA in hours and DEC in degrees...
//     double RA2  = Angle.HOURS_FROM_RAD * Math.atan2 (x2[1], x2[0]);
//     if (RA2 < 0) {
//         RA2 += 24.0;
//     }
//     
//     double DEC2 = Angle.SafeArcSinInDegrees (x2[2]);
//
//     return new SphericalCoordinates (RA2, DEC2, eq.radius);
// }
 
//
//
 public static double AngleBetweenVectorsInDegrees (CartesianCoordinates va, CartesianCoordinates vb)
{
    CartesianCoordinates a = va.Normalize ();
    CartesianCoordinates b = vb.Normalize ();

    // The normalized dot product of two vectors is equal to the cosine of the angle between them.
    double dotprod = a.x*b.x + a.y*b.y + a.z*b.z;
    double abs = Math.abs (dotprod);
    if (abs > 1.0) {
        if (abs > 1.00000001) {
            // Something is really messed up!  Avoid impossible Math.Acos() argument...
            // "Internal error: dot product = "
        	return 0;
        } else {
            // Assume that roundoff error has caused a valid dot product value to wander outside allowed values for Math.Acos()...
            return (dotprod < 0) ? 180.0 : 0.0;
        }
    } else {
        return DEG_FROM_RAD * Math.acos (dotprod);
    }
}
 
 public static double GreenwichSiderealTimeInHours (double day)
 {
     double midnight = Math.floor (day);    // discard fractional part to get same calendar day at midnight UTC
     double T0 = CenturiesSinceJ2000 (midnight);
     double tUT = (day - midnight) * 24.0;     // Greenwich time of day, in hours
     double SG = (6.6974 + 2400.0513 * T0) + (366.2422 / 365.2422) * tUT;
     SG = FixHours (SG);
     return SG;
 }
 public static double OblateLatitudeCorrection (double latitude)
 {
     // This function corrects for the flattening of the Earth due to its rotation.
     // Given a geographic latitude (which is defined based on the tilt of one's local horizon),
     // this function returns geocentric latitude (based on the angle between the equatorial plane and the location).
     // The correction is zero at the equator and at both poles, and is maximized at |latitude| = 45 degrees.
     // See:
     //      http://en.wikipedia.org/wiki/Latitude#Common_.22latitude.22
     //      http://en.wikipedia.org/wiki/Latitude#Geocentric_latitude
     return latitude - (0.1924 * SinDeg(2.0 * latitude));
 }

 public static double OblateRadiusCorrection (double latitude)
 {
     // Returns the fraction of equatorial Earth radius for sea level at the given geographic latitude.
     // This is due to flattening caused by Earth's rotation.
     // When latitude==0 (i.e. a point on the equator), the value returned is 1.0.
     // The value is minimized at latitude==+90 (North Pole) or latitude==-90 (South Pole).
     return 0.99833 + (0.00167 * CosDeg(2.0 * latitude));
 }

public static double jupiterMeanAnomaly(double d) {
	return 19.54334 + (0.08308 * d);
}

public static double saturnMeanAnomaly(double d) {
	return 316.9670 + (0.03344423 * d);
}

}
