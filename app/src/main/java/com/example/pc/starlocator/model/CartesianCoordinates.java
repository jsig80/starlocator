package com.example.pc.starlocator.model;

public class CartesianCoordinates {

	public double x;
	public double y;
	public double z;

	public CartesianCoordinates(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString() {
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}

	public double Distance() {
		return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
	}

	public CartesianCoordinates Normalize() {
		double r = this.Distance();
		if (r == 0.0) {
			// "Cannot normalize zero vector.";
			return null;
		}
		return new CartesianCoordinates(this.x / r, this.y / r, this.z / r);
	}

	public CartesianCoordinates Subtract(CartesianCoordinates other) {
		return new CartesianCoordinates(this.x - other.x, this.y - other.y, this.z - other.z);
	}

	public CartesianCoordinates Add(CartesianCoordinates other) {
		return new CartesianCoordinates(this.x + other.x, this.y + other.y, this.z + other.z);
	}
}
