package com.example.pc.starlocator;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Triangle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class CoordConvert {

    public static double k = 1.002737908;
    /**
     * Constant used by toRadians and toDegrees.
     */
    private static final double DEG_TO_RAD = 0.017453292519943295;
    public static double jd = 0;

    public static void findJd() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int julianYear = year;
        int julianMonth = month + 1;
        if (julianMonth < 3) {
            julianYear--;
            julianMonth += 12;
        }

        int a = julianYear / 100;
        int b = 2 - a + (a / 4);
        int c = (int) (365.25 * julianYear);
        int e = (int) (30.6001 * (julianMonth + 1));
        jd = b + c + day + e + 1720994.5;
    }

    public static double[] calcHorizontalCoordinates(double obsLon, double obsLat, double rightAsc, double decl, double horizonCorrectionInArcMinutes) {
        // http://en.wikipedia.org/wiki/Horizontal_coordinate_system
        double day=AstrUtil.DayValue();
        if (horizonCorrectionInArcMinutes == 0) {
            horizonCorrectionInArcMinutes = -34.0;
        }

        double GST = AstrUtil.GreenwichSiderealTimeInHours(day);
        double LST = GST + (obsLon / 15.0);
        double hourAngle = AstrUtil.FixHours(LST - rightAsc);

        double sinLat = AstrUtil.SinDeg(obsLat);
        double cosLat = AstrUtil.CosDeg(obsLat);

        double sinHourAngle = AstrUtil.SinHour(hourAngle);
        double cosHourAngle = AstrUtil.CosHour(hourAngle);

        double sinDec = AstrUtil.SinDeg(decl);
        double cosDec = AstrUtil.CosDeg(decl);
        double altitude = 0;
        double azimuth = 0;
        double altitudeRatio = (sinLat * sinDec) + (cosLat * cosDec * cosHourAngle);
        // Correct for values that are out of range for inverse sine function...
        double absAltitudeRatio = Math.abs(altitudeRatio);
        if (absAltitudeRatio > 1.0) {
            if (absAltitudeRatio > 1.000001) {
                // This is an excessive amount of error: something must be wrong
                // with the formula!
                // "Internal error: altitude would be a complex number!");
            } else {
                // Just correct for apparent roundoff without going bezerk.
                altitude = (altitudeRatio < 0) ? -90.0 : +90.0;
                azimuth = 180.0; // doesn't really matter what angle we assign:
                // use this value to assist culmination
                // algorithm.
            }
        } else {
            altitude = AstrUtil.DEG_FROM_RAD * Math.asin(altitudeRatio);
            double absAltitude = Math.abs(altitude);
            double ANGLE_CORRECTION_WINDOW = 6.0; // I chose 6 degrees as the
            // refraction cutoff,
            // because that is used for
            // Civil Twilight, which
            // should not be corrected.
            if ((absAltitude < ANGLE_CORRECTION_WINDOW) && (horizonCorrectionInArcMinutes != 0.0)) {
                // http://www.jgiesen.de/refract/index.html For more technical
                // details.
                // Correct for atmospheric lensing making the object appear
                // higher than it would if the Earth had no atmosphere.
                // We want the correction to maximize its effect at the horizon,
                // and vanish to zero as |altitude| approaches
                // ANGLE_CORRECTION_WINDOW.
                // For simplicity we apply a linear interpolation within this
                // range of altitudes.
                double linearFactor = (ANGLE_CORRECTION_WINDOW - absAltitude) / ANGLE_CORRECTION_WINDOW;
                altitude -= (horizonCorrectionInArcMinutes / 60.0) * linearFactor;
            }

            azimuth = AstrUtil.FixDegrees(
                    AstrUtil.AtanDeg2(-cosDec * sinHourAngle, (cosLat * sinDec) - (sinLat * cosDec * cosHourAngle)));
        }
        double[] result = new double[2];
        result[0] = azimuth;
        result[1] = altitude;
       // result[2] = LST;
        return result;
    }

    public static String decimalToDegrees(double degrees) {
        String result = "";
        double degrees_ = degrees;
        if (degrees < 0) {
            degrees_ = -1 * degrees;
            result += "-";
        }

        result += Math.floor(degrees_) + " deg ";
        double min = (degrees_ % 1) * 60;
        result += Math.floor(min) + " min ";
        double sec = (min % 1) * 60;
        result += Math.floor(sec) + " sec ";
        return result;
    }

    public static String decimalToHours(double degrees) {
        String result = "";
        double degrees_ = degrees;
        if (degrees < 0) {
            degrees_ = -1 * degrees;
            result += "-";
        }

        result += Math.floor(degrees_) + " Hours ";
        double min = (degrees_ % 1) * 60;
        result += Math.floor(min) + " min ";
        double sec = (min % 1) * 60;
        result += Math.floor(sec) + " sec ";
        return result;
    }

    // DD.MMSS to degrees: HH.MMSS to HH.HHHHH
    public static double degreesToDcecimal(double degrees) {
        double a1, a2, a3, mm, sgn0, ss;
        double degrees_ = degrees;
        sgn0 = 1;
        if (degrees < 0) {
            degrees_ = -1 * degrees;
            sgn0 = -1;
        }
        a1 = Math.floor(degrees_);
        mm = (degrees_ - a1) * 100;
        a2 = Math.floor(mm);
        ss = (mm - a2) * 100;
        a3 = ss;

        double result = sgn0 * (a1 + a2 / 60 + a3 / 3600);
        BigDecimal bdresult = new BigDecimal(String.valueOf(result)).setScale(10, BigDecimal.ROUND_HALF_UP);
        return bdresult.doubleValue();
    }

    // double array to decimal
    public static double degreesToDcecimal(double[] degrees) {
        double a1, a2, a3, mm, sgn0, ss;
        double degrees_ = degrees[0];
        sgn0 = 1;
        if (degrees[0] < 0) {
            degrees_ = -1 * degrees[0];
            sgn0 = -1;
        }
        a1 = Math.floor(degrees_);
        mm = degrees[1];
        a2 = Math.floor(mm);
        ss = degrees[2];
        a3 = ss;
        double result = sgn0 * (a1 + a2 / 60 + a3 / 3600);
        BigDecimal bdresult = new BigDecimal(String.valueOf(result)).setScale(10, BigDecimal.ROUND_HALF_UP);
        return bdresult.doubleValue();
    }

    public static double toRadians(double angdeg) {
        return angdeg * DEG_TO_RAD;
    }

    public static double toDegrees(double angrad) {
        return angrad / DEG_TO_RAD;
    }

    // declination in decimal
    public static double refraction(int temp, int pressure, double dec) {
        double dec_ = ((21.5 * pressure) / (271 + temp)) * (1.0 / Math.tan(toRadians(dec)));
        double mm = Math.floor(Math.abs(dec_ / 60));
        double ss = dec_ - mm * 60;
        dec_ = mm / 100 + ss / 10000;
        dec_ = degreesToDcecimal(dec_);
        dec = dec_ + dec;
        return dec;
    }

    // ra and dec in degrees, returns decimal hours and degrees
    public static double[] epoch(int oepoch, int nepoch, double e_ra, double e_dec) {

        double mm = 0;
        double ss = 0;

        double ra = e_ra;
        double dec = e_dec;

        double ra_ = 3.3 * (nepoch - oepoch) * (Math.cos(toRadians(23.5))
                + Math.sin(toRadians(23.5)) * Math.sin(toRadians(ra)) * Math.tan(toRadians(dec)));
        mm = Math.floor(Math.abs(ra_ / 60));
        ss = ra_ - mm * 60;
        ra_ = mm / 100 + ss / 10000;
        ra_ = degreesToDcecimal(ra_) * 15;
        ra = ra + ra_;

        double dec_ = 50 * (nepoch - oepoch) * Math.sin(toRadians(23.5)) * Math.cos(toRadians(ra));
        mm = Math.floor(Math.abs(dec_ / 60));
        ss = dec_ - mm * 60;
        dec_ = mm / 100 + ss / 10000;
        dec_ = degreesToDcecimal(dec_);
        dec = dec + dec_;

        double[] result = new double[2];
        result[0] = ra / 15;
        result[1] = dec;
        return result;
    }

    //ra and dec in degrees, returns decimal hours and degrees
    public static double[] convertJ2000ToJNow( double e_ra, double e_dec ){
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (jd == 0)
            findJd();
        double t = (jd - 2451545.0) / 36525;
        double t2 = t * t;
        double t3 = t2 * t;

        double zeta = toRadians((2306.2181 * t) + (0.30188 * t2) + (0.017998 * t3))/3600;
        double z = toRadians((2306.2181 * t) + (1.09468 * t2) + (0.018203 * t3))/3600;
        double theta = toRadians((2004.3109 * t) - (0.42665 * t2) - (0.041833 * t3))/3600;

        double raBefore=toRadians(e_ra);
        double declBefore=toRadians(e_dec);
        double A = Math.cos(declBefore) * Math.sin(raBefore + zeta);
        double B = (Math.cos(theta) * Math.cos(declBefore) * Math.cos(raBefore + zeta)) - (Math.sin(theta) * Math.sin(declBefore));
        double C = (Math.sin(theta) * Math.cos(declBefore) * Math.cos(raBefore + zeta)) - (Math.cos(theta) * Math.sin(declBefore));

        double ra = toDegrees(Math.atan2(A, B) + z);
        double decl=toDegrees(Math.acos(Math.sqrt((A * A) + (B * B))));    //toDegrees(Math.asin(C));

        if ( ra < 0.0 )
            ra += 360;
        else if ( ra>360 )
            ra -= 360;

        if ( (decl* e_dec) < 0.0 )
            decl = -decl;

        double[] result = new double[2];
        result[0] = ra/15;
        result[1] = decl;
        return result;
    }

    //gets azimuth and return horizontal angle
    public static double rev(double x) {
        return 360-x;
    }

    public static double[] getAllignedAltLat(double[][] tMatrix, double rightAsc, double decl, double t0) {

        double L, M, N,l,m,n;
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));

        float ut = cal.get(Calendar.HOUR_OF_DAY);
        float min = cal.get(Calendar.MINUTE);
        float sec = cal.get(Calendar.SECOND);
        float dec = ((min * 60) + sec) / 3600;
        ut += dec;
        double tRad = toRadians((ut) * 15);

        L = Math.cos(decl) * Math.cos(rightAsc - k * (tRad - t0));
        M = Math.cos(decl) * Math.sin(rightAsc - k * (tRad - t0));
        N = Math.sin(decl);
        double[][] matrix1 = new double[3][1];
        matrix1[0][0] = L;
        matrix1[1][0] = M;
        matrix1[2][0] = N;

        double[][] matrix2 = multiply(tMatrix, matrix1);
        l=matrix2[0][0];
        m=matrix2[1][0];
        n=matrix2[2][0];
        double vLength=Math.sqrt(Math.pow(l,2)+Math.pow(m,2)+Math.pow(n,2));
        double radAz = Math.atan2(m/vLength,l/vLength);
        double radAlt = Math.asin(n/vLength);

        double[] result = new double[2];
        result[0] = rev(toDegrees(radAz));
        result[1] = toDegrees(radAlt);
        if (result[0] < 0)
            result[0] += 360;
        if (result[0] >360)
            result[0] -= 360;
        return result;
    }

    // all input in radians
    public static double[][] allignList(List<Double[]> allignStars,double targetRightAsc, double targetDecl, double t0) {
        if(allignStars.size()<2)
            return null;
        else if(allignStars.size()==2)
            return allign(allignStars.get(0),allignStars.get(1),t0);
        else {
            double minDistance=Double.MAX_VALUE;
            int index1 = 0,index2 = 0,index3 = 0;
            //find closer points
            Coordinate target=new Coordinate();
            target.x = Math.cos(targetDecl) * Math.cos(targetRightAsc);
            target.y = Math.cos(targetDecl) * Math.sin(targetRightAsc);
            target.z = Math.sin(targetDecl);
            for (int i=0;i<allignStars.size();i++) {
                Double[] star=new Double[3];
                star[0] = Math.cos(allignStars.get(i)[2]) * Math.cos(allignStars.get(i)[1]);
                star[1] = Math.cos(allignStars.get(i)[2]) * Math.sin(allignStars.get(i)[1]);
                star[2] = Math.sin(allignStars.get(i)[2]);
                Coordinate coord=new Coordinate(star[0],star[1],star[2]);
                double distance = coord.distance3D(target);
                if(distance<minDistance){
                    index1=i;
                    minDistance=distance;
                }
            }
            minDistance=Double.MAX_VALUE;
            for (int i=0;i<allignStars.size();i++) {
                Double[] star=new Double[3];
                star[0] = Math.cos(allignStars.get(i)[2]) * Math.cos(allignStars.get(i)[1]);
                star[1] = Math.cos(allignStars.get(i)[2]) * Math.sin(allignStars.get(i)[1]);
                star[2] = Math.sin(allignStars.get(i)[2]);
                Coordinate coord=new Coordinate(star[0],star[1],star[2]);
                double distance = coord.distance3D(target);
                if(distance<minDistance &&i!=index1){
                    index2=i;
                    minDistance=distance;
                }
            }
            minDistance=Double.MAX_VALUE;
            for (int i=0;i<allignStars.size();i++) {
                Double[] star=new Double[3];
                star[0] = Math.cos(allignStars.get(i)[2]) * Math.cos(allignStars.get(i)[1]);
                star[1] = Math.cos(allignStars.get(i)[2]) * Math.sin(allignStars.get(i)[1]);
                star[2] = Math.sin(allignStars.get(i)[2]);
                Coordinate coord=new Coordinate(star[0],star[1],star[2]);
                double distance = coord.distance3D(target);
                if(distance<minDistance && i!=index1 && i!=index2){
                    index3=i;
                    minDistance=distance;
                }
            }
            return allignThree(allignStars.get(index1),allignStars.get(index2),allignStars.get(index3),t0);
        }
    }
    // all input in radians
    public static double[][] allignThree(Double[] star1, Double[] star2, Double[] star3, double t0) {

        double l1, m1, n1, L1, M1, N1;
        double l2, m2, n2, L2, M2, N2;
        double l3, m3, n3, L3, M3, N3;
        double[][] tMatrix;
        double[][] matrix1 = new double[3][3];
        double[][] matrix2 = new double[3][3];

        l1 = Math.cos(star1[4]) * Math.cos(star1[3]);
        m1 = Math.cos(star1[4]) * Math.sin(star1[3]);
        n1 = Math.sin(star1[4]);

        l2 = Math.cos(star2[4]) * Math.cos(star2[3]);
        m2 = Math.cos(star2[4]) * Math.sin(star2[3]);
        n2 = Math.sin(star2[4]);

        L1 = Math.cos(star1[2]) * Math.cos(star1[1] - k * (star1[0] - t0));
        M1 = Math.cos(star1[2]) * Math.sin(star1[1] - k * (star1[0] - t0));
        N1 = Math.sin(star1[2]);

        L2 = Math.cos(star2[2]) * Math.cos(star2[1] - k * (star2[0] - t0));
        M2 = Math.cos(star2[2]) * Math.sin(star2[1] - k * (star2[0] - t0));
        N2 = Math.sin(star2[2]);

        l3 = Math.cos(star3[4]) * Math.cos(star3[3]);
        m3 = Math.cos(star3[4]) * Math.sin(star3[3]);
        n3 = Math.sin(star3[4]);

        L3 = Math.cos(star3[2]) * Math.cos(star3[1] - k * (star3[0] - t0));
        M3 = Math.cos(star3[2]) * Math.sin(star3[1] - k * (star3[0] - t0));
        N3 = Math.sin(star3[2]);

        matrix1[0][0] = L1;
        matrix1[0][1] = L2;
        matrix1[0][2] = L3;
        matrix1[1][0] = M1;
        matrix1[1][1] = M2;
        matrix1[1][2] = M3;
        matrix1[2][0] = N1;
        matrix1[2][1] = N2;
        matrix1[2][2] = N3;

        matrix1 = invert(matrix1);

        matrix2[0][0] = l1;
        matrix2[0][1] = l2;
        matrix2[0][2] = l3;
        matrix2[1][0] = m1;
        matrix2[1][1] = m2;
        matrix2[1][2] = m3;
        matrix2[2][0] = n1;
        matrix2[2][1] = n2;
        matrix2[2][2] = n3;

        tMatrix = multiply(matrix2, matrix1);
        return tMatrix;
    }

    // all input in radians
    public static double[][] allign(Double[] star1, Double[] star2, double t0) {

        double l1, m1, n1, L1, M1, N1;
        double l2, m2, n2, L2, M2, N2;
        double l3, m3, n3, L3, M3, N3;
        double[][] tMatrix;
        double[][] matrix1 = new double[3][3];
        double[][] matrix2 = new double[3][3];

        l1 = Math.cos(star1[4]) * Math.cos(star1[3]);
        m1 = Math.cos(star1[4]) * Math.sin(star1[3]);
        n1 = Math.sin(star1[4]);

        l2 = Math.cos(star2[4]) * Math.cos(star2[3]);
        m2 = Math.cos(star2[4]) * Math.sin(star2[3]);
        n2 = Math.sin(star2[4]);

        L1 = Math.cos(star1[2]) * Math.cos(star1[1] - k * (star1[0] - t0));
        M1 = Math.cos(star1[2]) * Math.sin(star1[1] - k * (star1[0] - t0));
        N1 = Math.sin(star1[2]);

        L2 = Math.cos(star2[2]) * Math.cos(star2[1] - k * (star2[0] - t0));
        M2 = Math.cos(star2[2]) * Math.sin(star2[1] - k * (star2[0] - t0));
        N2 = Math.sin(star2[2]);

        double fract = 1 / Math
                .sqrt(Math.pow(m1 * n2 - n1 * m2, 2) + Math.pow(n1 * l2 - l1 * n2, 2) + Math.pow(l1 * m2 - m1 * l2, 2));
        l3 = fract * (m1 * n2 - n1 * m2);
        m3 = fract * (n1 * l2 - l1 * n2);
        n3 = fract * (l1 * m2 - m1 * l2);

        double fract1 = 1 / Math
                .sqrt(Math.pow(M1 * N2 - N1 * M2, 2) + Math.pow(N1 * L2 - L1 * N2, 2) + Math.pow(L1 * M2 - M1 * L2, 2));
        L3 = fract1 * (M1 * N2 - N1 * M2);
        M3 = fract1 * (N1 * L2 - L1 * N2);
        N3 = fract1 * (L1 * M2 - M1 * L2);
        matrix1[0][0] = L1;
        matrix1[0][1] = L2;
        matrix1[0][2] = L3;
        matrix1[1][0] = M1;
        matrix1[1][1] = M2;
        matrix1[1][2] = M3;
        matrix1[2][0] = N1;
        matrix1[2][1] = N2;
        matrix1[2][2] = N3;

        matrix1 = invert(matrix1);

        matrix2[0][0] = l1;
        matrix2[0][1] = l2;
        matrix2[0][2] = l3;
        matrix2[1][0] = m1;
        matrix2[1][1] = m2;
        matrix2[1][2] = m3;
        matrix2[2][0] = n1;
        matrix2[2][1] = n2;
        matrix2[2][2] = n3;

        tMatrix = multiply(matrix2, matrix1);
        return tMatrix;
    }

    public static double[][] invert(double a[][]){

        int n = a.length;

        double x[][] = new double[n][n];

        double b[][] = new double[n][n];

        int index[] = new int[n];

        for (int i = 0; i < n; ++i)

            b[i][i] = 1;

        // Transform the matrix into an upper triangle

        gaussian(a, index);

        // Update the matrix b[i][j] with the ratios stored

        for (int i = 0; i < n - 1; ++i)

            for (int j = i + 1; j < n; ++j)

                for (int k = 0; k < n; ++k)

                    b[index[j]][k]

                            -= a[index[j]][i] * b[index[i]][k];

        // Perform backward substitutions

        for (int i = 0; i < n; ++i)

        {

            x[n - 1][i] = b[index[n - 1]][i] / a[index[n - 1]][n - 1];

            for (int j = n - 2; j >= 0; --j)

            {

                x[j][i] = b[index[j]][i];

                for (int k = j + 1; k < n; ++k)

                {

                    x[j][i] -= a[index[j]][k] * x[k][i];

                }

                x[j][i] /= a[index[j]][j];

            }

        }

        return x;

    }

    public static void gaussian(double a[][], int index[]) {

        int n = index.length;
        double c[] = new double[n];
        // Initialize the index
        for (int i = 0; i < n; ++i)
            index[i] = i;
        // Find the rescaling factors, one from each row
        for (int i = 0; i < n; ++i) {
            double c1 = 0;
            for (int j = 0; j < n; ++j) {
                double c0 = Math.abs(a[i][j]);
                if (c0 > c1)
                    c1 = c0;
            }
            c[i] = c1;
        }
        // Search the pivoting element from each column
        int k = 0;
        for (int j = 0; j < n - 1; ++j) {
            double pi1 = 0;
            for (int i = j; i < n; ++i) {
                double pi0 = Math.abs(a[index[i]][j]);
                pi0 /= c[index[i]];
                if (pi0 > pi1) {
                    pi1 = pi0;
                    k = i;
                }
            }
            // Interchange rows according to the pivoting order
            int itmp = index[j];
            index[j] = index[k];
            index[k] = itmp;
            for (int i = j + 1; i < n; ++i) {
                double pj = a[index[i]][j] / a[index[j]][j];
                // Record pivoting ratios below the diagonal
                a[index[i]][j] = pj;
                // Modify other elements accordingly
                for (int l = j + 1; l < n; ++l)
                    a[index[i]][l] -= pj * a[index[j]][l];
            }
        }
    }

    public static double[][] multiply(double[][] a, double[][] b) {
        int maxI = a.length;
        int maxJ = b[0].length;
        int maxK = a[0].length;

        // creating another matrix to store the multiplication of two matrices
        double c[][] = new double[maxI][maxJ];

        // multiplying and printing multiplication of 2 matrices
        for (int i = 0; i < maxI; i++) {
            for (int j = 0; j < maxJ; j++) {
                c[i][j] = 0;
                for (int k = 0; k < maxK; k++) {
                    c[i][j] += a[i][k] * b[k][j];
                } // end of k loop
            } // end of j loop
        }
        return c;
    }


}
