package com.example.pc.starlocator.model;

public class Sun extends Body {

	public Sun(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent,Body earth) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}

	public Sun() {
		super();
		this.commonName="Sun";
	}

	@Override
	public CartesianCoordinates calcEclipticCartesianCoordinates(double day) {
		return new CartesianCoordinates(0.0, 0.0, 0.0);
	}

	@Override
	public SphericalCoordinates calcEclipticAngularCoordinates(double day,CartesianCoordinates ccoords) {
			return new SphericalCoordinates(0.0, 0.0, 0.0);
	}

	@Override
	public double DistanceFromEarth() {
		return eclipticCartesianCoordinates.Distance();
	}

	@Override
	public double DistanceFromSun() {
		return 0.0;
	}

	public double RadiusInMeters = 6.96e+8;

	@Override
	public double VisualMagnitude(double day) {
		double e = DistanceFromEarth();
		return -26.73 + (5.0 * Math.log10(e));
	}

}
