package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Pluto extends Body {

	public Pluto(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}

	public Pluto() {
		super();
		this.commonName="Pluto";
	}

	@Override
	public CartesianCoordinates calcEclipticCartesianCoordinates(double day) {
		double S = 50.03 + (0.033459652 * day);
		double P = 238.95 + (0.003968789 * day);

		double lonecl = 238.9508 + (0.00400703 * day) - 19.799 * AstrUtil.SinDeg(P) + 19.848 * AstrUtil.CosDeg(P)
				+ 0.897 * AstrUtil.SinDeg(2 * P) - 4.956 * AstrUtil.CosDeg(2 * P) + 0.610 * AstrUtil.SinDeg(3 * P)
				+ 1.211 * AstrUtil.CosDeg(3 * P) - 0.341 * AstrUtil.SinDeg(4 * P) - 0.190 * AstrUtil.CosDeg(4 * P)
				+ 0.128 * AstrUtil.SinDeg(5 * P) - 0.034 * AstrUtil.CosDeg(5 * P) - 0.038 * AstrUtil.SinDeg(6 * P)
				+ 0.031 * AstrUtil.CosDeg(6 * P) + 0.020 * AstrUtil.SinDeg(S - P) - 0.010 * AstrUtil.CosDeg(S - P);

		double latecl = -3.9082 - 5.453 * AstrUtil.SinDeg(P) - 14.975 * AstrUtil.CosDeg(P)
				+ 3.527 * AstrUtil.SinDeg(2 * P) + 1.673 * AstrUtil.CosDeg(2 * P) - 1.051 * AstrUtil.SinDeg(3 * P)
				+ 0.328 * AstrUtil.CosDeg(3 * P) + 0.179 * AstrUtil.SinDeg(4 * P) - 0.292 * AstrUtil.CosDeg(4 * P)
				+ 0.019 * AstrUtil.SinDeg(5 * P) + 0.100 * AstrUtil.CosDeg(5 * P) - 0.031 * AstrUtil.SinDeg(6 * P)
				- 0.026 * AstrUtil.CosDeg(6 * P) + 0.011 * AstrUtil.CosDeg(S - P);

		double r = 40.72 + 6.68 * AstrUtil.SinDeg(P) + 6.90 * AstrUtil.CosDeg(P) - 1.18 * AstrUtil.SinDeg(2 * P)
				- 0.03 * AstrUtil.CosDeg(2 * P) + 0.15 * AstrUtil.SinDeg(3 * P) - 0.14 * AstrUtil.CosDeg(3 * P);

		double coslon = AstrUtil.CosDeg(lonecl);
		double sinlon = AstrUtil.SinDeg(lonecl);
		double coslat = AstrUtil.CosDeg(latecl);
		double sinlat = AstrUtil.SinDeg(latecl);

		double xp = r * coslon * coslat;
		double yp = r * sinlon * coslat;
		double zp = r * sinlat;

		return new CartesianCoordinates(xp, yp, zp); 
	}
	
	@Override
	public double VisualMagnitude(double day) {

		double s = DistanceFromSun ();
		double e = DistanceFromEarth ();
        return 14.0 + (5.0 *Math.log10 ((e * s) / (31.97177 * 31.982)));   // a hack that ignores phase angle, based on data from http://www.heavens-above.com
   
	}
}
