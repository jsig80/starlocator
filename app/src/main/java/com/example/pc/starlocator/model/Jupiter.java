package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Jupiter extends Body {

	public Jupiter(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}
	
	@Override
	public void calcPosition(GeograficCoordinates location,Earth earth) {
		double day = AstrUtil.DayValue();
		eclipticCartesianCoordinates=calcEclipticCartesianCoordinates(day);
		eclipticCartesianCoordinates= perturb (eclipticCartesianCoordinates.x,
				eclipticCartesianCoordinates.y, eclipticCartesianCoordinates.z, day);
		eclipticAngularCoordinates=calcEclipticAngularCoordinates(day, eclipticCartesianCoordinates);
		geocentricCoordinates=calcGeocentricCoordinates(day, eclipticCartesianCoordinates,earth);
		eqCoords=calcEqCoords(day, location);
		horCoords=calcHorizontalCoordinates(day, location);
		this.setMagnitude(VisualMagnitude(AstrUtil.DayValue()));
	}
	
	public Jupiter(){
		super();
		setBodyData(
		        "Jupiter",
		        5.20288700,  -0.00011607,    // ja0, jac = semi-major axis (AU), rate (AU/century)
		        0.04838624,  -0.00013253,    // je0, jec = eccentricity (1), (1/century)
		        1.30439695,   0.00183714,    // jI0, jIc = inclination (deg), (deg/century)
		        34.39644051, 3034.74612775,  // jL0, jLc = mean longitude (deg), (deg/century)
		        14.72847983,  0.21252668,    // ju0, juc = longitude of perihelion (deg), (deg/century)
		        100.47390909, 0.20469106,    // jQ0, jQc = longitude of ascending node (deg), (deg/century)
		        -9.25, 0.014, 0, 0     // visual magnitude elements)       
		    );
	}
	@Override
	public CartesianCoordinates perturb(double xh, double yh, double zh, double d) {
		SphericalCoordinates ecliptic = AstrUtil.EclipticLatLon(xh, yh, zh);
		double lonecl = ecliptic.longitude;
		double latecl = ecliptic.latitude;

		double r = Math.sqrt(xh * xh + yh * yh + zh * zh);
		double Mj = AstrUtil.jupiterMeanAnomaly(d);
		double Ms = AstrUtil.saturnMeanAnomaly(d);

		lonecl += -0.332 * AstrUtil.SinDeg(2 * Mj - 5 * Ms - 67.6) - 0.056 * AstrUtil.SinDeg(2 * Mj - 2 * Ms + 21)
				+ 0.042 * AstrUtil.SinDeg(3 * Mj - 5 * Ms + 21) - 0.036 * AstrUtil.SinDeg(Mj - 2 * Ms)
				+ 0.022 * AstrUtil.CosDeg(Mj - Ms) + 0.023 * AstrUtil.SinDeg(2 * Mj - 3 * Ms + 52)
				- 0.016 * AstrUtil.SinDeg(Mj - 5 * Ms - 69);

		double coslon = AstrUtil.CosDeg(lonecl);
		double sinlon = AstrUtil.SinDeg(lonecl);
		double coslat = AstrUtil.CosDeg(latecl);
		double sinlat = AstrUtil.SinDeg(latecl);

		double xp = r * coslon * coslat;
		double yp = r * sinlon * coslat;
		double zp = r * sinlat;

		return new CartesianCoordinates(xp, yp, zp);
	}

}
