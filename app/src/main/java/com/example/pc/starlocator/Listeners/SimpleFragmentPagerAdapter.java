package com.example.pc.starlocator.Listeners;

import com.example.pc.starlocator.AlignFragment;
import com.example.pc.starlocator.MainActivity;
import com.example.pc.starlocator.ManualFragment;
import com.example.pc.starlocator.SettingsFragment;
import com.example.pc.starlocator.TrackingFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class SimpleFragmentPagerAdapter extends FragmentStateAdapter {
    final int PAGE_COUNT = 2;


    public SimpleFragmentPagerAdapter(FragmentActivity fa) {
        super(fa);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0)
            return new SettingsFragment();
       else
            return new TrackingFragment();
    }

    @Override
    public int getItemCount() {
        return PAGE_COUNT;
    }
}