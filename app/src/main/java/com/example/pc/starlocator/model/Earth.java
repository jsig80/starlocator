package com.example.pc.starlocator.model;

import com.example.pc.starlocator.AstrUtil;

public class Earth extends Body {

	public Earth(String name, double N0, double Nc, double i0, double ic, double w0, double wc, double a0, double ac,
			double e0, double ec, double M0, double Mc, double magBase, double magPhaseFactor,
			double magNonlinearFactor, double magNonlinearExponent) {
		super(name, N0, Nc, i0, ic, w0, wc, a0, ac, e0, ec, M0, Mc, magBase, magPhaseFactor, magNonlinearFactor,
				magNonlinearExponent);
	}

	public Earth() {
	}
	
	@Override
	public void calcPosition(GeograficCoordinates location,Earth earth) {
		double day = AstrUtil.DayValue();
		eclipticCartesianCoordinates=calcEclipticCartesianCoordinates(day);
		eclipticCartesianCoordinates= perturb (eclipticCartesianCoordinates.x,
				eclipticCartesianCoordinates.y, eclipticCartesianCoordinates.z, day);
		eclipticAngularCoordinates=calcEclipticAngularCoordinates(day, eclipticCartesianCoordinates);
		geocentricCoordinates=calcGeocentricCoordinates(day, eclipticCartesianCoordinates,null);
		eqCoords=calcEqCoords(day, location);
		horCoords=calcHorizontalCoordinates(day, location);
	}

	@Override
	public CartesianCoordinates calcEclipticCartesianCoordinates(double day) {
		// We use formulas for finding the Sun as seen from Earth,
		// then negate the (x,y,z) coordinates obtained to get the Earth's
		// position
		// from the Sun's perspective.

		// http://www.astro.uio.no/~bgranslo/aares/calculate.html <== Note error
		// in formula for DS, using sin(RS) where it should say sin(LS)
		// http://www.meteorobs.org/maillist/msg09197.html <== Correct formulas,
		// more accurate (complex)

		// These formulas use 'd' based on days since 1/Jan/2000 12:00 UTC
		// ("J2000.0"), instead of 0/Jan/2000 0:00 UTC ("day value").
		// Correct by subtracting 1.5 days...
		double d = day - 1.5;
		double T = d / 36525.0; // Julian centuries since J2000.0
		double L0 = 280.46645 + (36000.76983 * T) + (0.0003032 * T * T);
		// Sun's mean longitude, in degrees
		double M0 = 357.52910 + (35999.05030 * T) - (0.0001559 * T * T) - (0.00000048 * T * T * T);
		// Sun's mean anomaly, in degrees

		double C = // Sun's equation of center in degrees
				(1.914600 - 0.004817 * T - 0.000014 * T * T) * AstrUtil.SinDeg(M0)
						+ (0.01993 - 0.000101 * T) * AstrUtil.SinDeg(2 * M0) + 0.000290 * AstrUtil.SinDeg(3 * M0);

		double LS = L0 + C; // true ecliptical longitude of Sun

		double e = 0.016708617 - T * (0.000042037 + (0.0000001236 * T));
		double distanceInAU = (1.000001018 * (1 - e * e)) / (1 + e * AstrUtil.CosDeg(M0 + C));
		double x = -distanceInAU * AstrUtil.CosDeg(LS);
		double y = -distanceInAU * AstrUtil.SinDeg(LS);
		return new CartesianCoordinates(x, y, 0.0);
		// the Earth's center is
		// always on the plane of
		// the ecliptic (z=0), by
		// definition!
	}

	@Override
	public CartesianCoordinates calcGeocentricCoordinates(double day, CartesianCoordinates ccoords,Earth earth){ 
		return new CartesianCoordinates(0.0, 0.0, 0.0);
	}

	@Override
	public double DistanceFromEarth() {
		return 0.0; // included for completeness and consistency of planet
					// interface
	}

	@Override
	public double DistanceFromSun() {
		return eclipticCartesianCoordinates.Distance();
	}

	@Override
	public double VisualMagnitude(double day) {
		// "Cannot calculate visual magnitude for Earth!";
		return 0;
	}

}
